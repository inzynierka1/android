package me.engineer.android.objects;

import io.realm.annotations.PrimaryKey;

/**
 * Created by Bartosz Frankowski on 02.08.2017
 */

public class Token {
    @PrimaryKey
    private String id = "1";
    private String token;

    @SuppressWarnings("unused")
    public Token() {
    }

    public Token(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }
}
