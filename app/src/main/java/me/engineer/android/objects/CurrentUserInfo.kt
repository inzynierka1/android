package me.engineer.android.objects

/**
 * Created by Bartosz Frankowski on 08.11.2017
 */
data class CurrentUserInfo(val id: String = "", val email: String = "")