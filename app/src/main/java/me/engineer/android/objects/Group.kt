package me.engineer.android.objects

/**
 * Created by Bartosz Frankowski on 07.10.2017
 */

data class Group(val id: String = "", val name: String = "", val members: List<GroupMember> = emptyList()) {
    override fun toString(): String {
        return name
    }

    data class GroupMember(val id: String, val email: String)
}
