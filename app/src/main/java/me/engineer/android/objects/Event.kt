package me.engineer.android.objects

import org.joda.time.DateTime

/**
 * Created by Bartosz Frankowski on 19.09.2017
 */
data class Event(
        val eventId: String = "",
        val userId: String = "",
        val title: String,
        val content: String,
        val timestamp: DateTime? = null,
        val images: List<EventImage>? = emptyList(),
        val groups: List<String>? = emptyList(),
        val currentUserIsOwner: Boolean = false,
        val ownerEmail: String = "") {


    fun imageUrls(): List<String> = images?.map { it.url }.orEmpty()

    data class EventImage(
            val id: String,
            val url: String,
            val name: String = "")


    fun getOwner(): String {
        return userId
    }
}
