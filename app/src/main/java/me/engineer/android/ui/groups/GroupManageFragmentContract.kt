package me.engineer.android.ui.groups

import me.engineer.android.common.CommonContract
import me.engineer.android.objects.Group

/**
 * Created by Bartosz Frankowski on 17.10.2017
 */
interface GroupManageFragmentContract {
    interface View: CommonContract.CommonViewContract {
        fun displayGroups(groups: List<Group>)
        fun groupDeleteSuccess()
    }

    interface Presenter: CommonContract.CommonPresenterContract<View> {
        fun getGroups()
        fun removeGroup(id: String)
        fun editGroup(group: Group)
    }
}