package me.engineer.android.ui.auth;

import android.support.annotation.NonNull;
import android.util.Log;

import io.reactivex.android.schedulers.AndroidSchedulers;
import me.engineer.android.db.RealmRequest;
import me.engineer.android.db.objects.RealmToken;
import me.engineer.android.net.RetrofitProvider;
import me.engineer.android.providers.NetworkOnlyProviderWithSave;
import me.engineer.android.util.Encoder;
import me.engineer.android.util.RxUtil;

/**
 * Created by Bartosz Frankowski on 31.07.2017
 */

class LoginFragmentPresenterImpl implements LoginFragmentContract.Presenter {
    private static final String TAG = LoginFragmentPresenterImpl.class.getSimpleName();
    private LoginFragmentContract.View view;

    @Override
    public void attach(LoginFragmentContract.View view) {
        this.view = view;
    }

    @Override
    public void attemptLogin(String email, String password) {
        String encodedCredentials = base64EncodeCredentials(email, password);

        Log.d(TAG, "Attempt login with: " + email + ":" + password);
        NetworkOnlyProviderWithSave aNew = new NetworkOnlyProviderWithSave();
        aNew.request(RealmRequest.of(RealmToken.class),
                restService -> restService.login(encodedCredentials))
                .doOnNext(token -> Log.d(TAG, "Login success. Token: " + token.getToken()))
                .doOnNext(token -> RetrofitProvider.Companion.getInstance().setAuthentication(token))
                .doOnError(t -> Log.e(TAG, "Login failed: " + t.getMessage()))
                .doOnError(t -> RetrofitProvider.Companion.getInstance().removeAuthentication())
                .compose(RxUtil.applySchedulers())
                .compose(RxUtil.handleProgressView(view))
                .subscribe(token -> view.onLoginSuccess(), view::onLoginFailure);
    }

    @NonNull
    private String base64EncodeCredentials(String email, String password) {
        return Encoder.base64Encode(email + ":" + password);
    }

    @Override
    public void detach() {
        view = null;
    }
}
