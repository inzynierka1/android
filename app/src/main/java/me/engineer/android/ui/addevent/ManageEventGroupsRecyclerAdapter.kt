package me.engineer.android.ui.addevent

import android.content.Context
import android.graphics.Color
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import me.engineer.android.R
import me.engineer.android.objects.Group

/**
 * Created by Bartosz Frankowski on 07.11.2017
 */
class ManageEventGroupsRecyclerAdapter(val context: Context, var groups: List<DisplayedGroup>) :
        RecyclerView.Adapter<ManageEventGroupsRecyclerAdapter.ManageEventGroupsViewHolder>() {

    var filterGroupsBy: String = ""
        set(value) {
            field = value
            updateDisplayedGroups()
        }
    private var displayedGroups: List<DisplayedGroup> = groups

    override fun onBindViewHolder(holder: ManageEventGroupsViewHolder?, position: Int) {
        holder?.groupNameText?.text = displayedGroups[position].name()
        when (displayedGroups[position]) {
            is DisplayedGroup.AddedGroup -> {
                holder?.groupButton?.text = context.getString(R.string.activity_magane_event_recycler_button_remove)
                holder?.groupButton?.setBackgroundColor(Color.parseColor("#d9534f"))
                holder?.groupButton?.setOnClickListener {
                    val selectedItem = displayedGroups[position]
                    groups = groups - selectedItem + DisplayedGroup.NotAddedGroup(selectedItem.group)
                    updateDisplayedGroups()
                }
            }
            else -> {
                holder?.groupButton?.text = context.getString(R.string.activity_magane_event_recycler_button_add)
                holder?.groupButton?.setBackgroundColor(Color.parseColor("#5cb85c"))
                holder?.groupButton?.setOnClickListener {
                    val selectedItem = displayedGroups[position]
                    groups = groups - selectedItem + DisplayedGroup.AddedGroup(selectedItem.group)
                    updateDisplayedGroups()
                }
            }
        }
    }

    private fun updateDisplayedGroups() {
        displayedGroups = groups.filter {
            if (filterGroupsBy.isNotBlank()) it.group.name.toLowerCase().startsWith(filterGroupsBy) else true
        }.sortedWith(compareBy({ it is DisplayedGroup.NotAddedGroup }, { it.name() }))

        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ManageEventGroupsViewHolder {
        val view = LayoutInflater.from(parent!!.context)
                .inflate(R.layout.fragment_groups_add_recycler_users_item, parent, false)

        return ManageEventGroupsViewHolder(view)
    }

    override fun getItemCount(): Int = displayedGroups.size

    fun getAddedGroupIds(): List<String> = groups.filter { it is DisplayedGroup.AddedGroup }.map { it.group.id }

    inner class ManageEventGroupsViewHolder(view: View?) : RecyclerView.ViewHolder(view) {
        val groupNameText: TextView?
                = view?.findViewById(R.id.fragment_groups_add_recycler_users_item_email)

        val groupButton: TextView?
                = view?.findViewById(R.id.fragment_groups_add_recycler_users_item_button)
    }

    sealed class DisplayedGroup(val group: Group) {

        fun name(): String = group.name!!

        class AddedGroup(group: Group) : DisplayedGroup(group)
        class NotAddedGroup(group: Group) : DisplayedGroup(group)
    }
}