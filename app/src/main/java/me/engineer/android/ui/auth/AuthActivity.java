package me.engineer.android.ui.auth;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ProgressBar;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.engineer.android.R;
import me.engineer.android.common.ProgressBarCompatible;
import me.engineer.android.db.RealmDataManager;
import me.engineer.android.db.objects.RealmToken;
import me.engineer.android.net.RetrofitProvider;
import me.engineer.android.objects.Token;
import me.engineer.android.ui.MainActivity;

public class AuthActivity extends AppCompatActivity
        implements LoginFragment.OnLoginFragmentInteractionListener,
        RegisterFragment.OnRegisterFragmentInteractionListener, ProgressBarCompatible {

    @BindView(R.id.auth_activity_progress_bar)
    ProgressBar progressBarView;
    @BindView(R.id.auth_activity_frame)
    FrameLayout frameView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth);
        ButterKnife.bind(this);

        Token token = RealmDataManager.getNew()
                .get(RealmToken.class)
                .blockingFirst(new Token());

        if (token.getToken() != null) {
            RetrofitProvider.Companion.getInstance().setAuthentication(token);
            attemptMainActivityStart();
        } else {
            setupActivity();
        }
    }

    private void attemptMainActivityStart() {
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }

    private void setupActivity() {
        getSupportFragmentManager().beginTransaction().add(R.id.auth_activity_frame, new LoginFragment()).commit();
    }


    @Override
    public void switchToRegister() {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.auth_activity_frame, new RegisterFragment(), RegisterFragment.class.getSimpleName())
                .addToBackStack(RegisterFragment.class.getSimpleName())
                .commit();
    }

    @Override
    public void onLoginSuccess() {
        attemptMainActivityStart();
    }

    @Override
    public void switchToLogin() {
        FragmentManager manager = getSupportFragmentManager();
        if (manager.getBackStackEntryCount() > 0) manager.popBackStack();
        manager.beginTransaction()
                .replace(R.id.auth_activity_frame, new LoginFragment(), LoginFragment.class.getSimpleName())
                .addToBackStack(LoginFragment.class.getSimpleName())
                .commit();
    }

    @Override
    public void onRegisterSuccess() {
        attemptMainActivityStart();
    }


    @Override
    public void showProgress(boolean inProgress) {
        int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

        setFrameVisibility(inProgress, shortAnimTime);
        setProgressBarVisibility(inProgress, shortAnimTime);
    }

    private void setProgressBarVisibility(final boolean visible, int shortAnimTime) {
        progressBarView.setVisibility(visible ? View.VISIBLE : View.GONE);
        progressBarView.animate().setDuration(shortAnimTime).alpha(
                visible ? 1 : 0).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                progressBarView.setVisibility(visible ? View.VISIBLE : View.GONE);
            }
        });
    }

    private void setFrameVisibility(final boolean visible, int shortAnimTime) {
        frameView.setVisibility(visible ? View.GONE : View.VISIBLE);
        frameView.animate().setDuration(shortAnimTime).alpha(
                visible ? 0 : 1).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                frameView.setVisibility(visible ? View.GONE : View.VISIBLE);
            }
        });
    }
}
