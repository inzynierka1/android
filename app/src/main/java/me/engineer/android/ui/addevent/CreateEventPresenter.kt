package me.engineer.android.ui.addevent

import android.content.Context
import android.net.Uri
import android.util.Log
import com.ipaulpro.afilechooser.utils.FileUtils
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import me.engineer.android.App
import me.engineer.android.config.API
import me.engineer.android.db.RealmRequest
import me.engineer.android.db.objects.RealmEvent
import me.engineer.android.db.objects.RealmGroup
import me.engineer.android.net.RetrofitDataManager
import me.engineer.android.net.objects.EventRetrofitObject
import me.engineer.android.objects.Event
import me.engineer.android.providers.BaseDataProvider
import me.engineer.android.util.RxUtil
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File

class CreateEventPresenter(
        override var view: ManageEventActivityContract.View?,
        val context: Context) : ManageEventActivityContract.Presenter {
    override fun onEventManageConfirm(event: Event?, images: List<Uri>) {
        Log.i(TAG, "Confirmed adding event $event")

        if (event != null) {
            Log.d(TAG, "Event not null, performing add")
            if (event.eventId.isNotBlank()) {
                RetrofitDataManager.newInstance()
                        .request{ it.updateEvent(event.eventId, EventRetrofitObject.UpdateEvent.from(event))}
                        .doOnComplete{ Log.d(TAG, "Event successfully updated") }
                        .doOnComplete({ saveImages(event.userId, event.eventId, images) })
                        .applySchedulers()
                        .doOnError { view?.eventActionFailure() }
                        .doOnComplete{ view?.eventActionSuccess() }
                        .subscribe()

            } else {
                RetrofitDataManager.newInstance()
                        .request{it.addEvent(event)}
                        .doOnNext { Log.d(TAG, "Event successfully added") }
                        .doOnNext({ saveImages(it.userId, it.id, images) })
                        .applySchedulers()
                        .subscribe({ view?.eventActionSuccess() }, { view?.eventActionFailure() })
            }

        } else {
            Log.d(TAG, "Event in null, aborting")
            view?.eventActionNotPerformed()
        }
    }


    override fun getEventDetails(eventId: String) {
        BaseDataProvider().request(RealmRequest.of(RealmEvent::class.java, { it.eventId == eventId }), { it.getEventById(eventId) })
                .applySchedulers()
                .subscribe({ view?.onEventFetchSuccess(it) }, { view?.onEventFetchFailure() })
    }

    private fun saveImages(userId: String, eventId: String, images: List<Uri>) {
        val retrofitDataManager = RetrofitDataManager.newForEndpoint(API.CONTENT_ENDPOINT)

        val parts = images.map { FileUtils.getFile(context, it) }
                .zip(images, { file: File, image: Uri -> toPart(file, image) })

        Observable.fromIterable(parts)
                .flatMap { part -> retrofitDataManager.request { it.addImage(userId, eventId, part) } }
                .compose(RxUtil.applySchedulers())
                .subscribe()
    }

    override fun getAllGroups() {
        BaseDataProvider().requestAll(RealmRequest.of(RealmGroup::class.java), { it.groups })
                .applySchedulers()
                .subscribe({ view?.onGroupsFetchSuccess(it) }, { view?.onGroupFetchFailure() })
    }

    private fun toPart(file: File, uri: Uri): MultipartBody.Part {
        val type = MediaType.parse(App.getInstance().contentResolver.getType(uri)!!)
        val requestBody = RequestBody.create(type, file)
        return MultipartBody.Part.createFormData("file", file.name, requestBody)
    }

    companion object {
        val TAG: String = this::class.java.simpleName
    }

    fun <T> Observable<T>.applySchedulers(): Observable<T> {
        return subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }
}