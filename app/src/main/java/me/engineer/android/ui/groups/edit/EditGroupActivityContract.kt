package me.engineer.android.ui.groups.edit

import me.engineer.android.net.objects.UserHint
import me.engineer.android.objects.Group

/**
 * Created by Bartosz Frankowski on 10.11.2017
 */
interface EditGroupActivityContract {
    interface View {
        fun groupFetchSuccess(group: Group)
        fun groupFetchFailed()
        fun usersFetchSuccess(users: List<UserHint>)
        fun usersFetchFailed()
        fun groupSaveSuccess()
        fun groupSaveFailed()

    }

    interface Presenter {
        var view: View?

        fun attach(view: View) {
            this.view = view
        }

        fun detach() {
            this.view = null
        }

        fun getGroupInfo(groupId: String)
        fun getAllUsers()
        fun saveGroup(newGroup: Group)
    }}