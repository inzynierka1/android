package me.engineer.android.ui.eventslist

import me.engineer.android.common.CommonContract
import me.engineer.android.common.ProgressBarCompatible
import me.engineer.android.objects.Event

/**
 * Created by Bartosz Frankowski on 19.09.2017
 */

internal interface EventsListFragmentContract {
    interface View : CommonContract.CommonViewContract, ProgressBarCompatible {
        fun displayEvents(events: List<Event>)
    }

    interface Presenter : CommonContract.CommonPresenterContract<View> {
        fun getEventsToDisplay()
    }
}
