package me.engineer.android.ui.eventslist


import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.GridLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import kotlinx.android.synthetic.main.fragment_news_list.*
import me.engineer.android.R
import me.engineer.android.objects.Event


/**
 * A simple [Fragment] subclass.
 */
class NewsListFragment : Fragment(), EventsListFragmentContract.View {


    private val presenter: EventsListFragmentContract.Presenter = NewsPresenter()
    private var adapter: EventsAdapter? = null
    private var mListener: EventsListFragment.EventsListFragmentListener? = null

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val inflate = inflater!!.inflate(R.layout.fragment_news_list, container, false)
        presenter.attach(this)

        adapter = EventsAdapter(context)

        return inflate
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        fragment_news_recycler.layoutManager = GridLayoutManager(context, 1)
        fragment_news_recycler.itemAnimator = DefaultItemAnimator()
        adapter?.displayOwner = true
        fragment_news_recycler.adapter = adapter

        presenter.getEventsToDisplay()

        fragment_news_progress.setOnRefreshListener({ presenter.getEventsToDisplay() })
        fragment_news_fab.setOnClickListener({ this.onAddEventButtonClick() })
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is EventsListFragment.EventsListFragmentListener) {
            mListener = context
        } else {
            throw IllegalStateException("Activity should implement EventListFragmentListener")
        }
    }

    private fun onAddEventButtonClick() {
        mListener?.addEventButtonClicked()
    }

    override fun onResume() {
        super.onResume()
        presenter.getEventsToDisplay()
    }

    override fun onDetach() {
        super.onDetach()
        presenter.detach()
    }


    override fun showMessage(message: String?) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }

    override fun showProgress(inProgress: Boolean) {
        fragment_news_progress.isRefreshing = inProgress
    }

    override fun displayEvents(events: List<Event>) {
        adapter?.setEvents(events)
    }

}// Required empty public constructor
