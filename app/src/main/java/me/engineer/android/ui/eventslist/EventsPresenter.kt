package me.engineer.android.ui.eventslist

import me.engineer.android.db.RealmRequest
import me.engineer.android.db.objects.RealmCurentUserInfo
import me.engineer.android.db.objects.RealmEvent
import me.engineer.android.objects.CurrentUserInfo
import me.engineer.android.providers.BaseDataProvider
import me.engineer.android.providers.DbFirstSingleEmitProvider
import me.engineer.android.util.RxUtil

/**
 * Created by Bartosz Frankowski on 19.09.2017
 */

internal class EventsPresenter : EventsListFragmentContract.Presenter {
    private var view: EventsListFragmentContract.View? = null

    override fun attach(view: EventsListFragmentContract.View) {
        this.view = view
    }

    override fun detach() {
        view = null
    }

    override fun getEventsToDisplay() {

        val owner = DbFirstSingleEmitProvider().request(RealmRequest(RealmCurentUserInfo::class.java), { it.self }).blockingFirst(CurrentUserInfo())
        BaseDataProvider().requestAll(RealmRequest.of(RealmEvent::class.java, {it.userId == owner.id}), { it.onlyUsersEvents })
                .compose(RxUtil.applySchedulers())
                .doOnTerminate { view!!.showProgress(false) }
                .subscribe({ view!!.displayEvents(it) }) { t -> view!!.showMessage("Update failed") }
    }


}
