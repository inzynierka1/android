package me.engineer.android.ui.eventdetails

import android.util.Log
import io.reactivex.functions.BiFunction
import me.engineer.android.db.RealmDataManager
import me.engineer.android.db.RealmRequest
import me.engineer.android.db.objects.RealmCurentUserInfo
import me.engineer.android.db.objects.RealmEvent
import me.engineer.android.net.RetrofitDataManager
import me.engineer.android.objects.CurrentUserInfo
import me.engineer.android.objects.Event
import me.engineer.android.providers.BaseDataProvider
import me.engineer.android.providers.DbFirstSingleEmitProvider
import me.engineer.android.util.RxUtil

class EventDetailsActivityPresenterImpl : EventDetailsContract.Presenter {
    var view: EventDetailsContract.View? = null

    override fun attach(view: EventDetailsContract.View) {
        this.view = view
    }


    override fun getDetails(eventId: String) {
        BaseDataProvider()
                .request(RealmRequest(RealmEvent::class.java, { it.eventId == eventId }),
                        { restService -> restService.getEventById(eventId) })
                .zipWith(
                        DbFirstSingleEmitProvider().request(
                                RealmRequest(RealmCurentUserInfo::class.java),
                                { it.self }
                        ), BiFunction { event: Event, userInfo: CurrentUserInfo ->
                    event.copy(currentUserIsOwner = event.userId == userInfo.id)
                })
                .compose(RxUtil.applySchedulers())
                .subscribe({view?.displayEventDetails(it)}, {view?.showMessage("Failure")})
    }

    override fun deleteEvent(event: Event?) {
        Log.d(this::class.java.simpleName, "Deleting event ${event?.eventId}")
        RetrofitDataManager.newInstance()
                .request { it.deleteEvent(event?.eventId) }
                .doOnComplete { Log.d(this::class.java.simpleName, "Event successfully deleted") }
                .doOnComplete { RealmDataManager().removeFromRealm(event?.eventId, RealmEvent::class.java) }
                .compose(RxUtil.applySchedulers())
                .doOnComplete { view?.eventDeleteSuccess() }
                .doOnError { view?.eventDeleteFailure() }
                .subscribe()
    }

    override fun detach() {
        this.view = null
    }
}