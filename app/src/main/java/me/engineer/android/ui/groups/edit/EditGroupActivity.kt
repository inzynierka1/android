package me.engineer.android.ui.groups.edit

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.Menu
import android.view.MenuItem
import com.jakewharton.rxbinding2.widget.RxTextView
import kotlinx.android.synthetic.main.activity_edit_group.*
import kotlinx.android.synthetic.main.content_edit_group.*
import me.engineer.android.R
import me.engineer.android.common.showToast
import me.engineer.android.net.objects.UserHint
import me.engineer.android.objects.Group
import me.engineer.android.util.RxUtil

class EditGroupActivity : AppCompatActivity(), EditGroupActivityContract.View {

    private val presenter: EditGroupActivityContract.Presenter = EditGroupPresenterImpl(this, this)
    private var users: List<GroupMember> = emptyList()
    private var userHints: List<UserHint> = emptyList()
    private var userRecyclerAdapter: UserRecyclerAdapter? = null
    private var group: Group? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_group)
        setSupportActionBar(toolbar)

        intent.getStringExtra(GROUP_ID_EXTRA).let {
            if (it == null) {
                title = getString(R.string.activity_edit_group_title_add)
            } else {
                title = getString(R.string.activity_edit_group_title_edit)
                presenter.getGroupInfo(it)
            }
        }
        activity_edit_group_user_search_recycler.layoutManager = LinearLayoutManager(this)

        RxTextView.textChanges(activity_edit_group_user_search_input)
                .compose(RxUtil.applySchedulers())
                .subscribe {
                    userRecyclerAdapter?.filterUsersBy(activity_edit_group_user_search_input.text.toString())
                }

        presenter.getAllUsers()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.editgroup, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.activity_edit_group_menu_confirm -> saveGroup()
        }
        return true
    }

    fun saveGroup() {
        val newGroup = Group(id = group?.id.orEmpty(),
                name = activity_edit_group_group_name_input.text.toString(),
                members = userRecyclerAdapter?.getResultMembers().orEmpty())
        presenter.saveGroup(newGroup)

    }

    override fun groupSaveSuccess() {
        finish()
    }

    override fun groupSaveFailed() {
        showToast(getString(R.string.activity_edit_group_save_failed))
    }

    override fun groupFetchSuccess(group: Group) {
        this.group = group
        activity_edit_group_group_name_input.setText(group.name)
        updateUsers()
    }

    override fun usersFetchSuccess(users: List<UserHint>) {
        this.userHints = users
        updateUsers()
    }

    private fun updateUsers() {
        val members = group?.members?.map { it.id }.orEmpty()
        this.users = userHints.map {
            when {
                members.contains(it.userId) -> GroupMember.Member(it.userId, it.email)
                else -> GroupMember.NotMember(it.userId, it.email)
            }
        }
        userRecyclerAdapter = UserRecyclerAdapter(users, this)
        activity_edit_group_user_search_recycler.adapter = userRecyclerAdapter
    }

    override fun usersFetchFailed() {
        showToast(getString(R.string.activity_edit_users_fetch_failed))
    }

    override fun groupFetchFailed() {
        showToast(getString(R.string.activity_edit_group_fetch_failed))
    }

    companion object {
        const val GROUP_ID_EXTRA = "group_id_extra"
    }
}
