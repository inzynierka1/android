package me.engineer.android.ui.auth;

import android.util.Log;

import me.engineer.android.db.RealmRequest;
import me.engineer.android.db.objects.RealmToken;
import me.engineer.android.net.RetrofitProvider;
import me.engineer.android.net.objects.UseRequestBody;
import me.engineer.android.providers.NetworkOnlyProviderWithSave;
import me.engineer.android.util.RxUtil;

/**
 * Created by Bartosz Frankowski on 31.07.2017
 */

class RegisterFragmentPresenterImpl implements RegisterFragmentContract.Presenter {
    private static final String TAG = "RegisterFragmentPresent";

    private RegisterFragmentContract.View view;

    @Override
    public void attach(RegisterFragmentContract.View view) {
        this.view = view;
    }

    @Override
    public void attemptRegister(String email, String password) {
        Log.d(TAG, "Attempt registration with: " + email + " : " + password);
        NetworkOnlyProviderWithSave.getNew().request(RealmRequest.of(RealmToken.class),
                restService -> restService.register(new UseRequestBody(email, password)))
                .doOnComplete(() -> Log.d(TAG, "Registration success"))
                .doOnNext(token -> RetrofitProvider.Companion.getInstance().setAuthentication(token))
                .doOnError(t -> Log.e(TAG, "Registration failed: " + t.getMessage()))
                .doOnError(t -> RetrofitProvider.Companion.getInstance().removeAuthentication())
                .compose(RxUtil.applySchedulers())
                .compose(RxUtil.handleProgressView(view))
                .subscribe(token -> view.onRegisterSuccess(), view::onRegisterFailure)
        ;
    }

    @Override
    public void detach() {
        view = null;
    }
}
