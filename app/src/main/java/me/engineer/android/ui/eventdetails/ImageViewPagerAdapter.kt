package me.engineer.android.ui.eventdetails

import android.content.Context
import android.support.v4.view.PagerAdapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import com.squareup.picasso.Picasso
import me.engineer.android.R

/**
 * Created by Bartosz Frankowski on 11.10.2017
 */
class ImageViewPagerAdapter(val context: Context) : PagerAdapter() {

    val layoutInflater: LayoutInflater = LayoutInflater.from(context)
    var images: List<String> = emptyList()
        set(value) {
            field = value; notifyDataSetChanged()
        }

    override fun isViewFromObject(view: View?, obj: Any?) = view == obj

    override fun getCount() = images.size

    override fun instantiateItem(container: ViewGroup?, position: Int): Any {
        val itemView = layoutInflater
                .inflate(R.layout.activity_event_details_image_pager_element, container, false)

        val imageView = itemView
                .findViewById<ImageView>(R.id.activity_event_details_image_pager_image_view)

        if (images.isNotEmpty()) {
            Picasso.with(context)
                    .load(images[position])
                    .placeholder(R.drawable.placeholder)
                    .into(imageView)
        }



        container?.addView(itemView)
        return itemView
    }

    override fun destroyItem(container: ViewGroup?, position: Int, `object`: Any?) {
        container?.removeView(`object` as View)
    }
}