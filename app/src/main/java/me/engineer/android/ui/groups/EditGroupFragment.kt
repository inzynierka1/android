package me.engineer.android.ui.groups

import android.graphics.Color
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.jakewharton.rxbinding2.widget.RxTextView
import me.engineer.android.R
import me.engineer.android.common.bindView
import me.engineer.android.net.objects.UserHint
import me.engineer.android.objects.Group
import me.engineer.android.ui.groups.edit.GroupMember
import me.engineer.android.util.RxUtil

/**
 * Created by Bartosz Frankowski on 02.11.2017
 */
class EditGroupFragment : Fragment(), EditGroupFragmentContract.View {
    private val usersRecycler: RecyclerView by bindView(R.id.fragment_group_manage_groups_add_recycler)
    private val usersRecyclerAdapter = UsersRecyclerViewAdapter()

    private val groupNameEdit: EditText by bindView(R.id.fragment_group_manage_groups_add_group_name_input)
    private val userSearchInput: EditText by bindView(R.id.fragment_group_manage_groups_add_user_search_input)

    private val saveButton: Button by bindView(R.id.fragment_group_manage_groups_add_create_button)

    private val presenter: EditGroupFragmentContract.Presenter = EditGroupFragmentPresenterImpl()
    var group: Group = Group()
    var users: List<UserHint> = emptyList()

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater!!.inflate(R.layout.fragment_groups_add_group_fragment, container, false)

        presenter.attach(this)

        return view
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        usersRecycler.layoutManager = LinearLayoutManager(context)
        usersRecycler.adapter = usersRecyclerAdapter

        arguments.getString(BUNDLE_GROUP_ID).takeIf { it.isNotBlank() }?.let { presenter.getGroupDetails(it) }
        presenter.getAllUsers()


        RxTextView.textChanges(userSearchInput)
                .compose(RxUtil.applySchedulers())
                .subscribe {
                    if (it.isNotBlank()) {
                        displayUsersInAdapter(users.filter { user -> user.email.toLowerCase().startsWith(it.toString().toLowerCase()) })
                    } else {
                        displayUsersInAdapter(users)
                    }
                }

        saveButton.setOnClickListener({
            presenter.saveGroup(group)
            activity.supportFragmentManager.popBackStack()
        })

        RxTextView.textChanges(groupNameEdit)
                .compose(RxUtil.applySchedulers())
                .subscribe {
                    if (group.name != groupNameEdit.text.toString()) {
                        group = group.copy(name = groupNameEdit.text.toString())
                    }
                }
    }

    override fun showMessage(message: String?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onObtainDetailsFailure() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun displayGroupDetails(group: Group) {
        groupNameEdit.setText(group.name)
        displayUsersInAdapter(users)
    }

    override fun displayUsers(users: List<UserHint>) {
        this.users = users
        displayUsersInAdapter(users)
    }

    private fun displayUsersInAdapter(users: List<UserHint>) {
        val members = group.members.map { it.email }
        users.map { user ->
            when {
                members.contains(user.email) -> GroupMember.Member(user.userId, user.email)
                else -> GroupMember.NotMember(user.userId, user.email)
            }
        }
                .sortedWith(compareBy({ !members.contains(it.email) }, { it.email }))
                .let { usersRecyclerAdapter.items = it }
    }

    companion object {
        val BUNDLE_GROUP_ID = "edit_group_fragment_group_id"
    }



    //TODO get rid of inner
    inner class UsersRecyclerViewAdapter : RecyclerView.Adapter<UsersRecyclerViewAdapter.UsersRecyclerViewHolder>() {
        var items: List<GroupMember> = emptyList()
            set(value) {
                field = value
                notifyDataSetChanged()
            }

        override fun onBindViewHolder(holder: UsersRecyclerViewHolder?, position: Int) {
            holder?.userNameText?.text = items[position].email
            when (items[position]) {
                is GroupMember.Member -> {
                    holder?.userButton?.text = "Remove"
                    holder?.userButton?.setBackgroundColor(Color.parseColor("#d9534f"))
                    holder?.userButton?.setOnClickListener {
                        Toast.makeText(context, "Remove ${items[position].email}", Toast.LENGTH_LONG).show()
                        group = group.copy(members = group.members.filter { it.email != items[position].email })
                        userSearchInput.setText("")
                        displayUsers(users)
                    }
                }
                else -> {
                    holder?.userButton?.text = "Add"
                    holder?.userButton?.setBackgroundColor(Color.parseColor("#5cb85c"))
                    holder?.userButton?.setOnClickListener {
                        Toast.makeText(context, "Add ${items[position].email}", Toast.LENGTH_LONG).show()
                        group = group.copy(members = group.members + items[position].toMember())
                        userSearchInput.setText("")
                        displayUsers(users)
                    }
                }
            }
        }

        override fun getItemCount(): Int = items.size

        override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): UsersRecyclerViewHolder {
            val view = LayoutInflater.from(parent!!.context)
                    .inflate(R.layout.fragment_groups_add_recycler_users_item, parent, false)

            return UsersRecyclerViewHolder(view)
        }

        inner class UsersRecyclerViewHolder(view: View?) : RecyclerView.ViewHolder(view) {
            val userNameText: TextView?
                    = view?.findViewById(R.id.fragment_groups_add_recycler_users_item_email)

            val userButton: TextView?
                    = view?.findViewById(R.id.fragment_groups_add_recycler_users_item_button)
        }
    }
}