package me.engineer.android.ui.auth;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import me.engineer.android.R;
import me.engineer.android.common.ProgressBarCompatible;

import static me.engineer.android.util.Validators.isEmailValid;
import static me.engineer.android.util.Validators.isPasswordValid;

public class RegisterFragment extends Fragment implements RegisterFragmentContract.View {

    @BindView(R.id.fragment_register_edit_email)
    EditText emailEditText;
    @BindView(R.id.fragment_register_edit_password)
    EditText passwordEditText;
    @BindView(R.id.fragment_register_login_text)
    TextView loginLink;
    @BindView(R.id.fragment_register_register_button)
    Button registerButton;
    Unbinder unbinder;
    @BindView(R.id.fragment_register_edit_password_confirm)
    EditText confirmPasswordEditText;

    private RegisterFragmentContract.Presenter presenter = new RegisterFragmentPresenterImpl();
    private OnRegisterFragmentInteractionListener mListener;

    public RegisterFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_register, container, false);
        unbinder = ButterKnife.bind(this, view);
        presenter.attach(this);

        applyListeners();

        return view;
    }

    private void applyListeners() {
        // On keyboard done action
        confirmPasswordEditText.onEditorAction(EditorInfo.IME_ACTION_DONE);
        confirmPasswordEditText.setOnEditorActionListener((TextView textView, int i, KeyEvent a) -> onRegisterAction());

        // Register button click
        registerButton.setOnClickListener(v -> onRegisterAction());

        // Move to login fragment
        loginLink.setOnClickListener(v -> onLoginLinkLinkClick());
    }

    private boolean onRegisterAction() {
        String email = emailEditText.getText().toString();
        String password = passwordEditText.getText().toString();
        String passwordConfirm = confirmPasswordEditText.getText().toString();

        if (!isEmailValid(email)) {
            emailEditText.setError(getString(R.string.fragment_register_error_email_invalid));
            emailEditText.requestFocus();
        } else if (!isPasswordValid(password)) {
            passwordEditText.setError(getString(R.string.fragment_register_error_password_invalid));
            passwordEditText.requestFocus();
        } else if (!password.equals(passwordConfirm)) {
            confirmPasswordEditText.setError(getString(R.string.fragment_register_error_password_confirm_not_match));
        } else {
            presenter.attemptRegister(email, password);
            return false;
        }
        return true;
    }

    private void onLoginLinkLinkClick() {
        mListener.switchToLogin();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnRegisterFragmentInteractionListener) {
            mListener = (OnRegisterFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnRegisterFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
        presenter.detach();
    }

    @Override
    public void showMessage(String message) {
        Toast.makeText(this.getContext(), message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showProgress(boolean inProgress) {
        ((ProgressBarCompatible) mListener).showProgress(inProgress);
    }

    @Override
    public void onRegisterSuccess() {
        Toast.makeText(getContext(), getString(R.string.fragment_register_register_success),
                Toast.LENGTH_SHORT).show();
        mListener.onRegisterSuccess();
    }

    @Override
    public void onRegisterFailure(Throwable throwable) {
        Toast.makeText(getContext(), getString(R.string.fragment_register_register_failed),
                Toast.LENGTH_SHORT).show();
    }

    interface OnRegisterFragmentInteractionListener {
        void switchToLogin();

        void onRegisterSuccess();
    }
}
