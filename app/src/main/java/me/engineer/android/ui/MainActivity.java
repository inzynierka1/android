package me.engineer.android.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.engineer.android.R;
import me.engineer.android.db.RealmDataManager;
import me.engineer.android.db.RealmRequest;
import me.engineer.android.db.objects.RealmCurentUserInfo;
import me.engineer.android.net.RestService;
import me.engineer.android.providers.BaseDataProvider;
import me.engineer.android.ui.addevent.ManageEventActivity;
import me.engineer.android.ui.auth.AuthActivity;
import me.engineer.android.ui.eventslist.EventsListFragment;
import me.engineer.android.ui.eventslist.NewsListFragment;
import me.engineer.android.ui.groups.GroupManageFragment;
import me.engineer.android.util.RxUtil;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener,
        EventsListFragment.EventsListFragmentListener {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.nav_view)
    NavigationView navView;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;
    private Menu menu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        setupNavigationView();

        new BaseDataProvider().request(RealmRequest.of(RealmCurentUserInfo.class), RestService::getSelf)
                .compose(RxUtil.applySchedulers())
                .subscribe();

        getSupportFragmentManager().beginTransaction()
                .add(R.id.activity_main_frame, new NewsListFragment()).commit();

    }

    private void setupNavigationView() {
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setCheckedItem(R.id.nav_main_item);
    }

    private void logout() {
        RealmDataManager.getNew().cleanup();
        startActivity(new Intent(this, AuthActivity.class));
        finish();
    }


    //--------------------Control Components--------------------

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        this.menu = menu;
        return true;
    }

    @Override
    @SuppressWarnings("unused") //To be used in a future
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        item.setChecked(false);

        switch (id) {
            case R.id.nav_logout_item:
                logout();
                break;
            case R.id.nav_groups_item:
                switchFragmentTo(new GroupManageFragment(), "GroupManage");
                break;
            case R.id.nav_events_item:
                switchFragmentTo(new EventsListFragment(), "Events");
                break;
            case R.id.nav_main_item:
                goBackToMain();
                break;

        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void goBackToMain() {
        int backStackEntryCount = getSupportFragmentManager().getBackStackEntryCount();
        while (backStackEntryCount > 0) {
            getSupportFragmentManager().popBackStack();
            backStackEntryCount--;
        }
    }

    @Override
    public void addEventButtonClicked() {
//        switchFragmentTo(new AddEventFragment(), "AddEvent");
        startActivity(new Intent(this, ManageEventActivity.class));
    }

    private void switchFragmentTo(Fragment fragment, String name) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.activity_main_frame, fragment)
                .addToBackStack(name)
                .commit();
    }
}
