package me.engineer.android.ui.auth;

import me.engineer.android.common.CommonContract;
import me.engineer.android.common.ProgressBarCompatible;

/**
 * Created by Bartosz Frankowski on 31.07.2017
 */

interface RegisterFragmentContract {

    interface View extends CommonContract.CommonViewContract, ProgressBarCompatible{

        void onRegisterSuccess();

        void onRegisterFailure(Throwable throwable);
    }

    interface Presenter {
        void attach(View view);

        void attemptRegister(String email, String password);

        void detach();
    }
}
