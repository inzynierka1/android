package me.engineer.android.ui.groups

import me.engineer.android.db.RealmRequest
import me.engineer.android.db.objects.RealmGroup
import me.engineer.android.net.RetrofitDataManager
import me.engineer.android.net.objects.CreateGroupRequest
import me.engineer.android.objects.Group
import me.engineer.android.providers.BaseDataProvider
import me.engineer.android.util.RxUtil

class EditGroupFragmentPresenterImpl : EditGroupFragmentContract.Presenter {
    override fun saveGroup(group: Group) {
        RetrofitDataManager()
                .request { it.addNewGroup(CreateGroupRequest.from(group)) }
                .compose(RxUtil.applySchedulers())
                .subscribe()
    }

    private var view: EditGroupFragmentContract.View? = null

    override fun attach(view: EditGroupFragmentContract.View?) {
        this.view = view
    }

    override fun detach() {
        this.view = null
    }

    override fun getGroupDetails(groupId: String) {
        BaseDataProvider()
                .request(RealmRequest.of(RealmGroup::class.java, {it.id == groupId}), {it.getGroupDetails(groupId)})
                .compose(RxUtil.applySchedulers())
                .subscribe({view?.displayGroupDetails(it)}, {view?.onObtainDetailsFailure()})
    }

    override fun getAllUsers() {
        RetrofitDataManager()
                .request { it.allUserNames }
                .compose(RxUtil.applySchedulers())
                .subscribe({view?.displayUsers(it)}, {view?.onObtainDetailsFailure()})
    }
}