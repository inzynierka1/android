package me.engineer.android.ui.addevent

import android.Manifest
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import com.jakewharton.rxbinding2.widget.RxTextView
import com.tbruyelle.rxpermissions2.RxPermissions
import io.reactivex.Observable
import kotlinx.android.synthetic.main.activity_manage_event.*
import kotlinx.android.synthetic.main.content_manage_event.*
import me.engineer.android.R
import me.engineer.android.objects.Event
import me.engineer.android.objects.Group
import me.engineer.android.ui.addevent.ManageEventGroupsRecyclerAdapter.DisplayedGroup
import me.engineer.android.util.RxUtil

class ManageEventActivity : AppCompatActivity(), ManageEventActivityContract.View, AddEventImagesAdapter.OnAddImageInteractionListener {

    private var presenter: ManageEventActivityContract.Presenter? = CreateEventPresenter(this, this)

    private var event: Event? = null
    private var addedImages: List<Uri?> = emptyList()

    var groupsAdapter = ManageEventGroupsRecyclerAdapter(this, emptyList())

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_manage_event)
        setSupportActionBar(toolbar)

        presenter?.attach(this)

        intent.getStringExtra(EVENT_ID_EXTRA).let {
            if (it == null) {
                title = getString(R.string.activity_manage_event_title_add)
            } else {
                title = getString(R.string.activity_manage_event_title_edit)
                presenter?.getEventDetails(it)
            }
        }

        presenter?.getAllGroups()
        activity_event_manage_title_input.requestFocus()
        activity_event_manage_images_recycler.adapter = AddEventImagesAdapter(this, this, emptyList())
        activity_event_manage_groups_recycler.layoutManager = LinearLayoutManager(this)
        activity_event_manage_groups_recycler.adapter = groupsAdapter

        RxTextView.textChanges(activity_event_manage_groups_find_input)
                .compose(RxUtil.applySchedulers())
                .subscribe {
                    groupsAdapter.filterGroupsBy = activity_event_manage_groups_find_input.text.toString()
                }
    }

    override fun onEventFetchSuccess(event: Event) {
        this.event = event
        activity_event_manage_images_recycler.adapter = AddEventImagesAdapter(this, this, event.imageUrls())
        activity_event_manage_title_input.setText(event.title)
        activity_event_manage_content_input.setText(event.content)
    }

    override fun onEventFetchFailure() {
        showMessage(getString(R.string.activity_manage_event_error_failed_fetch))
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.manageevent, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.activity_event_manage_menu_confirm -> presenter?.onEventManageConfirm(createEventFromSettings(), addedImages.map { it!! })
        }
        return true
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_OK && requestCode == 1) {
            addedImages += data?.data
            activity_event_manage_images_recycler.adapter =
                    AddEventImagesAdapter(this, this, addedImages.map { it?.toString().orEmpty() })
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter?.detach()
    }

    // View Contract

    override fun eventActionSuccess() {
        showMessage(getString(R.string.activity_manage_event_success))
        finish()
    }

    override fun eventActionFailure() {
        showMessage(getString(R.string.activity_manage_event_failure), Toast.LENGTH_LONG)
    }

    override fun eventActionNotPerformed() {
        showMessage(getString(R.string.activity_manage_event_can_not_be_saved))
    }

    override fun onAddImageAction() {
        RxPermissions(this)
                .request(Manifest.permission.READ_EXTERNAL_STORAGE)
                .flatMap { handlePermission(it) }
                .subscribe({ chooseImage(it) }, { handleNoPermission(it) })
    }

    override fun onRemoveImageAction(image: String) {
        addedImages = addedImages.filter { it.toString() != image }
        event = event?.copy(images = event?.images?.filter { !image.contains(it.id) })
        activity_event_manage_images_recycler.adapter = AddEventImagesAdapter(this, this,
                addedImages.map { it?.toString().orEmpty() })
    }

    override fun onGroupsFetchSuccess(groups: List<Group>) {
        val results: List<DisplayedGroup> = groups.map {
            when {
                event?.groups?.contains(it.id) == true -> DisplayedGroup.AddedGroup(it)
                else -> DisplayedGroup.NotAddedGroup(it)
            }
        }
        groupsAdapter = ManageEventGroupsRecyclerAdapter(this, results)
        activity_event_manage_groups_recycler.adapter = groupsAdapter
    }


    override fun onGroupFetchFailure() {
        showMessage(getString(R.string.activity_manage_event_group_fetch_failed))
    }

    // Others

    private fun handleNoPermission(throwable: Throwable) {
        if (throwable is SecurityException) {
            Toast.makeText(this, getString(R.string.activity_manage_event_refused_permission), Toast.LENGTH_LONG).show()
        } else {
            Toast.makeText(this, getString(R.string.common_failure), Toast.LENGTH_SHORT).show()
        }
    }

    private fun handlePermission(granted: Boolean): Observable<Boolean> {
        return if (granted) {
            Observable.just(true)
        } else Observable.error(SecurityException("Permission READ_EXTERNAL_STORAGE not obtained"))
    }

    private fun chooseImage(booleanObservable: Boolean) {
        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT
        startActivityForResult(Intent.createChooser(intent,
                "Select Picture"), 1)
    }


    private fun createEventFromSettings(): Event? = event?.copy(
            title = activity_event_manage_title_input.text.toString(),
            content = activity_event_manage_content_input.text.toString(),
            groups = groupsAdapter.getAddedGroupIds()
    ) ?: Event(
            title = activity_event_manage_title_input.text.toString(),
            content = activity_event_manage_content_input.text.toString(),
            groups = groupsAdapter.getAddedGroupIds()
    )

    private fun showMessage(message: String, length: Int = Toast.LENGTH_SHORT) =
            Toast.makeText(this, message, length).show()

    companion object {
        val EVENT_ID_EXTRA = "manage_event_event_id"
    }

}
