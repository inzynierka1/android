package me.engineer.android.ui.eventdetails

import android.content.Intent
import android.os.Bundle
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_event_details.*
import me.engineer.android.R
import me.engineer.android.common.bindView
import me.engineer.android.objects.Event
import me.engineer.android.ui.addevent.ManageEventActivity

class EventDetailsActivity : AppCompatActivity(), EventDetailsContract.View {
    private val titleText: TextView by bindView(R.id.activity_event_details_title)
    private val contentText: TextView by bindView(R.id.activity_event_details_content)
    private val imageViewPager: ViewPager by bindView(R.id.activity_event_details_images)
    private var imageViewPagerAdapter: ImageViewPagerAdapter? = null
    var presenter: EventDetailsContract.Presenter? = null
    var event: Event? = null
    set(value) {
        field = value
        setupView()
    }
    var eventId: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_event_details)

        title = getString(R.string.activity_event_details_title)

        this.presenter = EventDetailsActivityPresenterImpl()
        presenter!!.attach(this)
        eventId = intent.getStringExtra("eventId")
        presenter!!.getDetails(eventId!!)

        imageViewPagerAdapter = ImageViewPagerAdapter(this)
        imageViewPager.adapter = imageViewPagerAdapter
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.eventdetails, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.activity_event_details_menu_edit -> {
                val intent = Intent(this, ManageEventActivity::class.java)
                intent.putExtra(ManageEventActivity.EVENT_ID_EXTRA, event?.eventId)
                startActivity(intent)
            }
            R.id.activity_event_details_menu_delete -> presenter?.deleteEvent(event)
        }
        return true
    }

    override fun onResume() {
        super.onResume()
        if (!eventId.isNullOrEmpty()) presenter?.getDetails(eventId!!)
    }

    override fun eventDeleteSuccess() {
        finish()
    }

    override fun eventDeleteFailure() {
        showMessage(getString(R.string.activity_event_details_delete_failed))
    }

    override fun displayEventDetails(event: Event) {
        this.event = event
        if (event.currentUserIsOwner) {
            setSupportActionBar(toolbar)
        }


    }

    private fun setupView() {
        imageViewPagerAdapter?.images = event?.imageUrls().orEmpty()
        titleText.text = event?.title
        contentText.text = event?.content

        if (event?.imageUrls()?.isEmpty() == true) {
            imageViewPager.visibility = View.GONE
        } else {
            imageViewPager.visibility = View.VISIBLE
        }
    }


    override fun showMessage(message: String) {
        Toast.makeText(applicationContext, message, Toast.LENGTH_SHORT).show()
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter?.detach()
    }
}
