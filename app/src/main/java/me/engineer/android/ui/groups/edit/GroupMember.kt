package me.engineer.android.ui.groups.edit

import me.engineer.android.objects.Group

/**
 * Created by Bartosz Frankowski on 10.11.2017
 */
sealed class GroupMember(val id: String, val email: String) {
    class Member(id: String, email: String) : GroupMember(id, email)
    class NotMember(id: String, email: String) : GroupMember(id, email)

    fun toMember() = Group.GroupMember(id, email)
}