package me.engineer.android.ui.groups.edit

import android.content.Context
import android.graphics.Color
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import me.engineer.android.R
import me.engineer.android.objects.Group

/**
 * Created by Bartosz Frankowski on 10.11.2017
 */
class UserRecyclerAdapter(private var users: List<GroupMember>, val context: Context) : RecyclerView.Adapter<UserRecyclerAdapter.UsersRecyclerViewHolder>() {

    var filterText: String = ""
    var usersDisplayed = users.sortedWith(compareBy({ it !is GroupMember.Member }, { it.email }))
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onBindViewHolder(holder: UsersRecyclerViewHolder?, position: Int) {
        holder?.userNameText?.text = usersDisplayed[position].email
        when (usersDisplayed[position]) {
            is GroupMember.Member -> {
                holder?.userButton?.text = context.getString(R.string.fragment_groups_add_recycler_button_remove)
                holder?.userButton?.setBackgroundColor(Color.parseColor("#d9534f"))
                holder?.userButton?.setOnClickListener {
                    val changedUser = usersDisplayed[position]
                    users = users - changedUser + GroupMember.NotMember(changedUser.id, changedUser.email)
                    displayNewUsers()
                }
            }
            else -> {
                holder?.userButton?.text = context.getString(R.string.fragment_groups_add_recycler_button_add)
                holder?.userButton?.setBackgroundColor(Color.parseColor("#5cb85c"))
                holder?.userButton?.setOnClickListener {
                    val changedUser = usersDisplayed[position]
                    users = users - changedUser + GroupMember.Member(changedUser.id, changedUser.email)
                    displayNewUsers()
                }
            }
        }
    }

    fun getResultMembers(): List<Group.GroupMember> {
        return users.filter { it is GroupMember.Member }.map { it.toMember() }
    }

    override fun getItemCount(): Int = usersDisplayed.size

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): UsersRecyclerViewHolder {
        val view = LayoutInflater.from(parent!!.context)
                .inflate(R.layout.fragment_groups_add_recycler_users_item, parent, false)

        return UsersRecyclerViewHolder(view)
    }

    inner class UsersRecyclerViewHolder(view: View?) : RecyclerView.ViewHolder(view) {
        val userNameText: TextView?
                = view?.findViewById(R.id.fragment_groups_add_recycler_users_item_email)

        val userButton: TextView?
                = view?.findViewById(R.id.fragment_groups_add_recycler_users_item_button)
    }

    fun filterUsersBy(text: String) {
        this.filterText = text
        displayNewUsers()
    }

    private fun displayNewUsers() {
        usersDisplayed = users.filter { it.email.toLowerCase().contains(filterText.toLowerCase()) }
                .sortedWith(compareBy({ it !is GroupMember.Member }, { it.email }))
    }
}