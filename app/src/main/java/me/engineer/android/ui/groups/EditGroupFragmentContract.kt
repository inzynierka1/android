package me.engineer.android.ui.groups

import me.engineer.android.common.CommonContract
import me.engineer.android.net.objects.UserHint
import me.engineer.android.objects.Group

/**
 * Created by Bartosz Frankowski on 17.10.2017
 */
interface EditGroupFragmentContract {
    interface View: CommonContract.CommonViewContract {
        fun displayGroupDetails(group: Group)
        fun displayUsers(users: List<UserHint>)
        fun onObtainDetailsFailure()
    }

    interface Presenter: CommonContract.CommonPresenterContract<View> {
        fun getGroupDetails(groupId: String)
        fun getAllUsers()
        fun saveGroup(group: Group)
    }
}