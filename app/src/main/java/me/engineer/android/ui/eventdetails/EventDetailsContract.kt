package me.engineer.android.ui.eventdetails

import me.engineer.android.common.CommonContract
import me.engineer.android.objects.Event

interface EventDetailsContract {
    interface View: CommonContract.CommonViewContract {
        fun displayEventDetails(event: Event)
        fun eventDeleteSuccess()
        fun eventDeleteFailure()
    }

    interface Presenter: CommonContract.CommonPresenterContract<View> {
        fun getDetails(eventId: String)
        fun deleteEvent(event: Event?)
    }
}