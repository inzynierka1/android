package me.engineer.android.ui.eventslist;


import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import me.engineer.android.R;
import me.engineer.android.objects.Event;

/**
 * A simple {@link Fragment} subclass.
 */
public class EventsListFragment extends Fragment implements EventsListFragmentContract.View {


    @BindView(R.id.fragment_events_recycler)
    RecyclerView fragmentEventsRecycler;
    @BindView(R.id.fragment_events_progress)
    SwipeRefreshLayout fragmentEventsProgress;
    Unbinder unbinder;
    @BindView(R.id.fragment_events_fab)
    FloatingActionButton addEventButton;
    private EventsListFragmentContract.Presenter eventListFragmentPresenter;
    private EventsAdapter adapter;
    private EventsListFragmentListener mListener;

    public EventsListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View inflate = inflater.inflate(R.layout.fragment_events_list, container, false);

        unbinder = ButterKnife.bind(this, inflate);

        eventListFragmentPresenter = new EventsPresenter();
        eventListFragmentPresenter.attach(this);

        adapter = new EventsAdapter(this.getContext());
        RecyclerView.LayoutManager gridLayoutManager = new GridLayoutManager(getContext(), 1);
        fragmentEventsRecycler.setLayoutManager(gridLayoutManager);
        fragmentEventsRecycler.setItemAnimator(new DefaultItemAnimator());
        fragmentEventsRecycler.setAdapter(adapter);

        eventListFragmentPresenter.getEventsToDisplay();

        fragmentEventsProgress.setOnRefreshListener(eventListFragmentPresenter::getEventsToDisplay);
        addEventButton.setOnClickListener(this::onAddEventButtonClick);
        return inflate;
    }

    private void onAddEventButtonClick(View view) {
        mListener.addEventButtonClicked();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof EventsListFragmentListener) {
            mListener = (EventsListFragmentListener) context;
        } else {
            throw new IllegalStateException("Activity should implement EventListFragmentListener");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        eventListFragmentPresenter.getEventsToDisplay();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void showMessage(String message) {

    }

    @Override
    public void showProgress(boolean inProgress) {
        fragmentEventsProgress.setRefreshing(inProgress);
    }

    @Override
    public void displayEvents(List<Event> events) {
        adapter.setEvents(events);
    }

    public interface EventsListFragmentListener {
        void addEventButtonClicked();
    }
}
