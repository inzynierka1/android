package me.engineer.android.ui.addevent

import android.net.Uri
import me.engineer.android.objects.Event
import me.engineer.android.objects.Group

/**
 * Created by Bartosz Frankowski on 04.11.2017
 */
interface ManageEventActivityContract {

    interface View {
        fun eventActionSuccess()
        fun eventActionFailure()
        fun eventActionNotPerformed()

        fun onGroupsFetchSuccess(groups: List<Group>)
        fun onGroupFetchFailure()

        fun onEventFetchSuccess(event: Event)
        fun onEventFetchFailure()
    }

    interface Presenter {
        var view: View?

        fun attach(view: View) {
            this.view = view
        }

        fun detach() {
            this.view = null
        }

        fun onEventManageConfirm(event: Event?, images: List<Uri>)
        fun getAllGroups()
        fun getEventDetails(eventId: String)
    }



}