package me.engineer.android.ui.groups.edit

import android.content.Context
import me.engineer.android.db.RealmRequest
import me.engineer.android.db.objects.RealmGroup
import me.engineer.android.net.RetrofitDataManager
import me.engineer.android.net.objects.CreateGroupRequest
import me.engineer.android.objects.Group
import me.engineer.android.providers.BaseDataProvider
import me.engineer.android.util.RxUtil

class EditGroupPresenterImpl(
        override var view: EditGroupActivityContract.View?,
        val context: Context) : EditGroupActivityContract.Presenter {
    override fun getGroupInfo(groupId: String) {
        BaseDataProvider()
                .request(RealmRequest.of(RealmGroup::class.java, {it.id == groupId}), {it.getGroupDetails(groupId)})
                .compose(RxUtil.applySchedulers())
                .subscribe({view?.groupFetchSuccess(it)}, {view?.groupFetchFailed()})
    }

    override fun getAllUsers() {
        RetrofitDataManager()
                .request { it.allUserNames }
                .compose(RxUtil.applySchedulers())
                .subscribe({view?.usersFetchSuccess(it)}, {view?.usersFetchFailed()})    }

    override fun saveGroup(newGroup: Group) {
        if(newGroup.id.isBlank()) {
            createNewGroup(newGroup)
        } else {
            updateGroup(newGroup)
        }
    }

    private fun updateGroup(newGroup: Group) {
        RetrofitDataManager()
                .request { it.updateGroup(newGroup.id, CreateGroupRequest.from(newGroup)) }
                .compose(RxUtil.applySchedulers())
                .doOnComplete { view?.groupSaveSuccess() }
                .doOnError { view?.groupSaveFailed() }
                .subscribe()
    }

    private fun createNewGroup(newGroup: Group) {
        RetrofitDataManager()
                .request { it.addNewGroup(CreateGroupRequest.from(newGroup)) }
                .compose(RxUtil.applySchedulers())
                .doOnComplete { view?.groupSaveSuccess() }
                .doOnError { view?.groupSaveFailed() }
                .subscribe()
    }

}