package me.engineer.android.ui.auth;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import me.engineer.android.R;
import me.engineer.android.common.ProgressBarCompatible;

import static me.engineer.android.util.Validators.isEmailValid;
import static me.engineer.android.util.Validators.isPasswordValid;

public class LoginFragment extends Fragment implements LoginFragmentContract.View {

    private LoginFragmentContract.Presenter presenter = new LoginFragmentPresenterImpl();
    private OnLoginFragmentInteractionListener mListener;

    @BindView(R.id.fragment_login_edit_email)
    EditText emailEditText;
    @BindView(R.id.fragment_login_edit_password)
    EditText passwordEditText;
    @BindView(R.id.fragment_login_register_text)
    TextView registerText;

    @BindView(R.id.fragment_login_login_button)
    Button loginButton;
    Unbinder unbinder;

    public LoginFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_login, container, false);
        unbinder = ButterKnife.bind(this, view);
        presenter.attach(this);

        applyListeners();

        return view;
    }

    private void applyListeners() {
        // On keyboard done action
        passwordEditText.onEditorAction(EditorInfo.IME_ACTION_DONE);
        passwordEditText.setOnEditorActionListener((TextView textView, int i, KeyEvent a) -> onLoginAction());

        // Login button click
        loginButton.setOnClickListener(v -> onLoginAction());

        // Move to register fragment
        registerText.setOnClickListener(v -> onRegisterLinkClick());
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnLoginFragmentInteractionListener) {
            mListener = (OnLoginFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnRegisterFragmentInteractionListener");
        }
        if (!(context instanceof ProgressBarCompatible)) {
            throw new RuntimeException(context.toString()
                    + " must implement ProgressBarCompatible");
        }
    }


    private boolean onLoginAction() {
        String email = emailEditText.getText().toString();
        String password = passwordEditText.getText().toString();

        if (!isEmailValid(email)) {
            emailEditText.setError(getString(R.string.fragment_login_error_email_invalid));
            emailEditText.requestFocus();
        } else if (!isPasswordValid(password)) {
            passwordEditText.setError(getString(R.string.fragment_login_error_password_invalid));
        } else {
            presenter.attemptLogin(email, password);
            return false;
        }
        return true;
    }

    private void onRegisterLinkClick() {
        mListener.switchToRegister();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        presenter.detach();
        unbinder.unbind();
    }

    // View Contract methods

    @Override
    public void showMessage(String message) {
        Toast.makeText(this.getContext(), message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showProgress(boolean inProgress) {
        ((ProgressBarCompatible) mListener).showProgress(inProgress);
    }

    @Override
    public void onLoginSuccess() {
        Toast.makeText(getContext(),
                getString(R.string.fragment_login_login_success),
                Toast.LENGTH_SHORT).show();
        mListener.onLoginSuccess();
    }

    @Override
    public void onLoginFailure(Throwable throwable) {
        Toast.makeText(getContext(), getString(R.string.fragment_login_login_failed),
                Toast.LENGTH_SHORT).show();
    }

    interface OnLoginFragmentInteractionListener{
        void switchToRegister();
        void onLoginSuccess();
    }
}
