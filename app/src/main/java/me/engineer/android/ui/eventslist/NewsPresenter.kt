package me.engineer.android.ui.eventslist

import android.util.Log
import io.reactivex.functions.BiFunction
import me.engineer.android.db.RealmRequest
import me.engineer.android.db.objects.RealmCurentUserInfo
import me.engineer.android.db.objects.RealmEvent
import me.engineer.android.net.RetrofitDataManager
import me.engineer.android.objects.CurrentUserInfo
import me.engineer.android.objects.Event
import me.engineer.android.providers.BaseDataProvider
import me.engineer.android.providers.DbFirstSingleEmitProvider
import me.engineer.android.util.RxUtil

/**
 * Created by Bartosz Frankowski on 13.11.2017
 */
internal class NewsPresenter : EventsListFragmentContract.Presenter {
    private var view: EventsListFragmentContract.View? = null

    override fun attach(view: EventsListFragmentContract.View) {
        this.view = view
    }

    override fun detach() {
        view = null
    }

    override fun getEventsToDisplay() {
        val owner = DbFirstSingleEmitProvider().request(RealmRequest(RealmCurentUserInfo::class.java), { it.self })
                .doOnNext{Log.d(this::class.java.simpleName, "Current user is $it")}
                .repeat(2)
                .onErrorReturnItem(CurrentUserInfo())


        val users = RetrofitDataManager()
                .request { it.allUserNames }
                .map { it.groupBy({ it.userId }, { it.email }) }
                .repeat(2)
                .onErrorReturn { emptyMap() }

        BaseDataProvider().requestAll(RealmRequest.of(RealmEvent::class.java), { it.allEvents })
                .doOnNext{Log.d(this::class.java.simpleName, "Found ${it.size} events")}
                .filter{ it.isNotEmpty() }
                .zipWith(owner,
                        BiFunction<List<Event>, CurrentUserInfo, List<Event>> { events, currentUser ->
                            Log.d(this::class.java.simpleName, "$events")
                            Log.d(this::class.java.simpleName, "$currentUser")
                            val map = events.map { it.copy(currentUserIsOwner = it.userId == currentUser.id) }
                            Log.d(this::class.java.simpleName, "$map")
                            map
                        })
                .zipWith(users, BiFunction<List<Event>, Map<String, List<String>>, List<Event>> { events, userIdMap ->
                    events.map { it.copy(ownerEmail = userIdMap[it.userId]?.firstOrNull().orEmpty()) }})
                .compose(RxUtil.applySchedulers())
                .doOnTerminate { view!!.showProgress(false) }
                .subscribe({ view!!.displayEvents(it) }) { t -> view!!.showMessage("Update failed") }
    }

}