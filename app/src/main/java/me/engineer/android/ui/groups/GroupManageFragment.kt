package me.engineer.android.ui.groups


import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import kotlinx.android.synthetic.main.fragment_group_manage.*
import me.engineer.android.R
import me.engineer.android.objects.Group
import me.engineer.android.ui.groups.edit.EditGroupActivity

class GroupManageFragment : Fragment(), GroupManageFragmentContract.View {
    private var presenter: GroupManageFragmentContract.Presenter? = null
    private var groups: List<Group>? = emptyList()
    private val groupsRecyclerAdapter = GroupsRecyclerViewAdapter()

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater!!.inflate(R.layout.fragment_group_manage, container, false)

        presenter = GroupManagePresenterImpl(context)
        presenter?.attach(this)
        presenter?.getGroups()

        return view
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        fragment_group_manage_groups_add_button.setOnClickListener({onAddGroupClick()})

        val linearLayoutManager = LinearLayoutManager(context)
        fragment_group_manage_groups_recycler.layoutManager = linearLayoutManager
        fragment_group_manage_groups_recycler.adapter = groupsRecyclerAdapter
        val dividerItemDecoration =
                DividerItemDecoration(fragment_group_manage_groups_recycler.context, linearLayoutManager.orientation)
        fragment_group_manage_groups_recycler.addItemDecoration(dividerItemDecoration)
    }

    override fun onResume() {
        super.onResume()
        presenter?.getGroups()
    }

    private fun onAddGroupClick() {
        context.startActivity(Intent(context, EditGroupActivity::class.java))
    }

    override fun groupDeleteSuccess() {
        Toast.makeText(context, getString(R.string.fragment_group_manage_remove_success), Toast.LENGTH_SHORT).show()
        presenter?.getGroups()
    }

    override fun displayGroups(groups: List<Group>) {
        this.groups = groups
        groupsRecyclerAdapter.items = groups
    }

    override fun onDetach() {
        presenter?.detach()
        super.onDetach()
    }

    override fun showMessage(message: String?) = TODO("not implemented")

    inner class GroupsRecyclerViewAdapter: RecyclerView.Adapter<GroupsRecyclerViewAdapter.GroupsRecyclerViewHolder>() {

        var items: List<Group> = emptyList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

        override fun onBindViewHolder(holder: GroupsRecyclerViewHolder?, position: Int) {
            holder?.groupNameText?.text = items[position].name
            holder?.groupMembersText?.text = getString(R.string.fragment_group_manage_members) + items[position].members.size
            holder?.removeGroupButton?.setOnClickListener { presenter?.removeGroup(items[position].id) }
            holder?.itemView?.setOnClickListener{ presenter?.editGroup(items[position])}

        }

        override fun getItemCount(): Int = items.size

        override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): GroupsRecyclerViewHolder {
            val view = LayoutInflater.from(parent!!.context)
                    .inflate(R.layout.fragment_groups_recycler_group_item, parent, false)

            return GroupsRecyclerViewHolder(view)
        }
        inner class GroupsRecyclerViewHolder(view: View?): RecyclerView.ViewHolder(view) {

            val groupNameText: TextView?
                    = view?.findViewById(R.id.fragment_groups_recycler_group_item_name)

            val groupMembersText: TextView?
                    = view?.findViewById(R.id.fragment_groups_recycler_group_item_members)
            val removeGroupButton: TextView?
                    = view?.findViewById(R.id.fragment_groups_recycler_group_item_remove_button)
        }

    }

}
