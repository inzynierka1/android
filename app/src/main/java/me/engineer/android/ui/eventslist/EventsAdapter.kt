package me.engineer.android.ui.eventslist

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.jakewharton.rxbinding2.view.RxView
import com.squareup.picasso.Picasso
import me.engineer.android.R
import me.engineer.android.R.id.*
import me.engineer.android.objects.Event
import me.engineer.android.ui.eventdetails.EventDetailsActivity
import java.util.*

/**
 * Created by Bartosz Frankowski on 19.09.2017
 */

class EventsAdapter(private val context: Context) : RecyclerView.Adapter<EventsAdapter.EventsViewHolder>() {
    private var events: List<Event> = ArrayList()
    var displayOwner: Boolean = false


    fun setEvents(events: List<Event>) {
        this.events = events.sortedByDescending { event -> event.timestamp }
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EventsViewHolder {
        val itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.event_card_layout, parent, false)
        return EventsViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: EventsViewHolder, position: Int) {
        holder.displayEvent(events[position])
    }

    override fun getItemCount(): Int = events.size

    inner class EventsViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        val sharedBy: TextView = view.findViewById(fragment_events_card_event_owner)
        val image: ImageView = view.findViewById(fragment_events_card_event_image)

        private var event: Event? = null

        fun displayEvent(event: Event) {
            this.event = event
            setupView()
        }

        private fun setupView() {
            if (displayOwner)  {
                sharedBy.visibility = View.VISIBLE
                if (event?.currentUserIsOwner == true) {
                    sharedBy.text = context.getString(R.string.fragment_events_card_event_shared_by_you)
                } else {
                    sharedBy.text = context.getString(R.string.fragment_events_card_event_shared_by)+ event?.ownerEmail
                }
            } else {
                sharedBy.visibility = View.GONE
            }
            view.findViewById<TextView>(fragment_events_card_event_title).text = event?.title
            view.findViewById<TextView>(fragment_events_card_event_date).text = event?.timestamp?.toString("dd/MM/yyyy HH:mm")
            view.findViewById<TextView>(fragment_events_card_event_content).text = event?.content
            setupImage(event)
        }


        init {
            RxView.clicks(view).subscribe { displayEventDetails() }
            RxView.clicks(image).subscribe { displayEventDetails() }
            RxView.longClicks(image).subscribe { setupImage(event) }
        }

        private fun displayEventDetails() {
            event.let {
                context.startActivity(Intent(context, EventDetailsActivity::class.java)
                        .putExtra("eventId", it?.eventId))
            }
        }

        private fun setupImage(event: Event?) {
            val url = event?.imageUrls()?.firstOrNull()

            if(url.isNullOrBlank()) {
                image.visibility = View.GONE
                image.setImageResource(0)
            } else {
                image.visibility = View.VISIBLE

                Picasso.with(context)
                        .load(url)
                        .placeholder(R.drawable.placeholder)
                        .resize(this.itemView.measuredWidth,
                                this.itemView.resources.getDimensionPixelOffset(R.dimen.card_image_max_heigth))
                        .onlyScaleDown()
                        .centerInside()
                        .into(image)
            }
        }
    }

}
