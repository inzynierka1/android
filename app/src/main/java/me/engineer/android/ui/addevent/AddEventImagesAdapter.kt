package me.engineer.android.ui.addevent

import android.content.Context
import android.support.v4.view.PagerAdapter
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.ImageView
import com.squareup.picasso.Picasso
import me.engineer.android.R

/**
 * Created by Bartosz Frankowski on 05.11.2017
 */
class AddEventImagesAdapter(val context: Context,
                            val listener: OnAddImageInteractionListener,
                            val images: List<String>): PagerAdapter() {

    override fun isViewFromObject(view: View?, obj: Any?) = view == obj


    override fun getCount(): Int = images.size + 1

    override fun instantiateItem(container: ViewGroup?, position: Int): Any {
        val itemView = LayoutInflater.from(context)
                .inflate(R.layout.activity_event_details_image_pager_element, container, false)


        val imageView = itemView
                .findViewById<ImageView>(R.id.activity_event_details_image_pager_image_view)
        val removeIcon = itemView
                .findViewById<ImageView>(R.id.activity_event_details_image_pager_image_view_remove)

        if (position == images.size) {
            displayPlaceholder(imageView)
            removeIcon.visibility = GONE
        } else {
            displayImage(images[position], imageView)
            removeIcon.visibility = VISIBLE
            removeIcon.setOnClickListener({listener.onRemoveImageAction(images[position])})
        }

        container?.addView(itemView)
        return itemView
    }

    private fun displayImage(imagePath: String, imageView: ImageView) {
        Picasso.with(context)
                .load(imagePath)
                .placeholder(R.drawable.placeholder)
                .into(imageView)

        imageView.setOnClickListener({})
    }

    private fun displayPlaceholder(imageView: ImageView) {
        Picasso.with(context)
                .load(R.drawable.ic_add_black_24dp)
                .into(imageView)

        imageView.setOnClickListener{listener.onAddImageAction()}
    }

    override fun destroyItem(container: ViewGroup?, position: Int, `object`: Any?) {
        container?.removeView(`object` as View)
    }



    interface OnAddImageInteractionListener {
        fun onAddImageAction()
        fun onRemoveImageAction(image: String)
    }

}