package me.engineer.android.ui.auth;

import me.engineer.android.common.CommonContract;
import me.engineer.android.common.ProgressBarCompatible;

/**
 * Created by Bartosz Frankowski on 31.07.2017
 */

interface LoginFragmentContract {
    interface View extends CommonContract.CommonViewContract, ProgressBarCompatible{
        void onLoginSuccess();

        void onLoginFailure(Throwable throwable);
    }

    interface Presenter extends CommonContract.CommonPresenterContract<View>{
        void attach(View view);

        void attemptLogin(String email, String password);

        void detach();

    }

}
