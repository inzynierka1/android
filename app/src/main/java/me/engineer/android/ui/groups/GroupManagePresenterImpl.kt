package me.engineer.android.ui.groups

import android.content.Context
import android.content.Intent
import me.engineer.android.db.RealmDataManager
import me.engineer.android.db.RealmRequest
import me.engineer.android.db.objects.RealmGroup
import me.engineer.android.net.RetrofitDataManager
import me.engineer.android.objects.Group
//import me.engineer.android.db.objects.RealmGroup
import me.engineer.android.providers.BaseDataProvider
import me.engineer.android.ui.groups.edit.EditGroupActivity
import me.engineer.android.util.RxUtil

class GroupManagePresenterImpl(val context: Context) : GroupManageFragmentContract.Presenter {
    private var view: GroupManageFragmentContract.View? = null

    override fun attach(view: GroupManageFragmentContract.View?) {
        this.view = view!!
    }

    override fun getGroups() {
        BaseDataProvider()
                .requestAll(RealmRequest.of(RealmGroup::class.java), {it.groups})
                .compose(RxUtil.applySchedulers())
                .subscribe({view?.displayGroups(it)})
    }

    override fun detach() {
        this.view = null
    }

    override fun removeGroup(id: String) {
        RetrofitDataManager.newInstance()
                .request { it.deleteGroup(id) }
                .compose(RxUtil.applySchedulers())
                .doOnComplete { RealmDataManager.getNew().removeFromRealm(id, RealmGroup::class.java) }
                .doOnComplete { view?.groupDeleteSuccess() }
                .subscribe()
    }

    override fun editGroup(group: Group) {
        val intent = Intent(context, EditGroupActivity::class.java)
        intent.putExtra(EditGroupActivity.GROUP_ID_EXTRA, group.id)
        context.startActivity(intent)
    }
}
