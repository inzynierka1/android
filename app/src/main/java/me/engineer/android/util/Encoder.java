package me.engineer.android.util;

/**
 * Created by Bartosz Frankowski on 24.08.2017
 */

public class Encoder {
    public static String base64Encode(String input) {
        return android.util.Base64.encodeToString(input.trim().getBytes(), android.util.Base64.DEFAULT).trim();
    }
}
