package me.engineer.android.util;

import org.apache.commons.validator.routines.EmailValidator;

/**
 * Created by Bartosz Frankowski on 31.07.2017
 */

public class Validators {
    public static boolean isPasswordValid(String password) {
        return PasswordValidator.isValid(password);
    }

    public static boolean isEmailValid(String email) {
        return EmailValidator.getInstance().isValid(email);
    }

    private static class PasswordValidator {
        public static final int MIN_PASSWORD_LENGTH = 3;
        public static final int MAX_PASSWORD_LENGTH = 50;

        static boolean isValid(String password){
            return password != null
                    && password.length() > MIN_PASSWORD_LENGTH
                    && password.length() < MAX_PASSWORD_LENGTH;
        }
    }
}
