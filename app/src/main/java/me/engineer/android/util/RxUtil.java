package me.engineer.android.util;

import java.util.List;

import io.reactivex.ObservableTransformer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Action;
import io.reactivex.functions.Predicate;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.Observable;
import me.engineer.android.common.ProgressBarCompatible;

/**
 * Created by Bartosz Frankowski on 24.08.2017
 */

public class RxUtil {

    public static <T> ObservableTransformer<T, T> applySchedulers() {
        return observable -> observable
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public static <T> ObservableTransformer<T, T> handleProgressView(ProgressBarCompatible view) {
        return observable -> observable
                .doOnSubscribe(ignored -> view.showProgress(true))
                .doOnTerminate(() -> view.showProgress(false));
    }

    public static <T> ObservableTransformer<List<T>, List<T>> filterListItems(Predicate<T> filter) {
        return observable -> observable
                .flatMap(Observable::fromIterable)
                .filter(filter)
                .toList()
                .toObservable();
    }
}
