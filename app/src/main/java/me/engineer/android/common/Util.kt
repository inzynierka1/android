package me.engineer.android.common

import android.support.v7.app.AppCompatActivity
import android.widget.Toast

/**
 * Created by Bartosz Frankowski on 10.11.2017
 */
fun AppCompatActivity.showToast(message: String, length: Int = Toast.LENGTH_SHORT) = Toast.makeText(this, message, length).show()