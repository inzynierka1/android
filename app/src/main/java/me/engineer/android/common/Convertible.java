package me.engineer.android.common;

/**
 * Created by Bartosz Frankowski on 02.08.2017
 */

public interface Convertible<T> {
    T convert();
}
