package me.engineer.android.common;

/**
 * Created by Bartosz Frankowski on 25.08.2017
 */

public interface ProgressBarCompatible {
    void showProgress(boolean inProgress);
}
