package me.engineer.android.common;

public class CommonContract {

    public interface CommonViewContract {
        void showMessage(String message);
    }

    public interface CommonPresenterContract<T extends CommonViewContract> {
        void attach(T view);
        void detach();
    }
}
