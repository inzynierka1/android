package me.engineer.android.net.objects

import me.engineer.android.net.ConvertibleRetrofitObject
import me.engineer.android.objects.CurrentUserInfo

/**
 * Created by Bartosz Frankowski on 08.11.2017
 */
class CurrentUserInfoRetrofit(val id: String, val email: String): ConvertibleRetrofitObject<CurrentUserInfo> {
    override fun convert(): CurrentUserInfo = CurrentUserInfo(id, email)
}