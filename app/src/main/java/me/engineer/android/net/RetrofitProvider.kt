package me.engineer.android.net

import me.engineer.android.config.API
import me.engineer.android.db.RealmDataManager
import me.engineer.android.db.objects.RealmToken
import me.engineer.android.objects.Token
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Created by Bartosz Frankowski on 04.07.2017
 */

class RetrofitProvider(private val clientProvider: HttpClientProvider,
                       private val gsonConverterFactory: GsonConverterFactory) {

    private object Holder {
        val INSTANCE = RetrofitProvider(HttpClientProvider.instance, GsonConverterFactory.create())
    }

    companion object {
        val instance: RetrofitProvider by lazy { Holder.INSTANCE }
    }

    private var authenticatedInstance: Retrofit? = null
    private val baseInstance: Retrofit = buildRetrofit()

    private fun buildRetrofit(endpoint: String = API.ENDPOINT, authToken: Token? = null): Retrofit {
        val client = if (authToken?.token.isNullOrEmpty())
            clientProvider.client else clientProvider.withAuth(authToken!!.token)

        return Retrofit.Builder()
                .baseUrl(endpoint)
                .addConverterFactory(gsonConverterFactory)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(client)
                .build()
    }

    fun setAuthentication(token: Token): Retrofit {
        authenticatedInstance = buildRetrofit(authToken = token)
        return authenticatedInstance!!
    }

    fun getRetrofitInstance(): Retrofit = if (authenticatedInstance == null)
        baseInstance else authenticatedInstance!!

    fun getCustomRetrofitInstance(endpoint: String = API.ENDPOINT, authToken: Token? = null): Retrofit =
            buildRetrofit(endpoint, authToken)

    fun removeAuthentication() {
        authenticatedInstance = null
    }

}
