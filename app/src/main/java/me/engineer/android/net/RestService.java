package me.engineer.android.net;

import java.util.List;

import io.reactivex.Observable;
import me.engineer.android.net.objects.CreateGroupRequest;
import me.engineer.android.net.objects.CurrentUserInfoRetrofit;
import me.engineer.android.net.objects.EventRetrofitObject;
import me.engineer.android.net.objects.GroupResponse;
import me.engineer.android.net.objects.TokenResponse;
import me.engineer.android.net.objects.UseRequestBody;
import me.engineer.android.net.objects.UserHint;
import me.engineer.android.objects.Event;
import okhttp3.MultipartBody;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;

/**
 * Created by Bartosz Frankowski on 04.07.2017
 */

public interface RestService {

    // Authentication

    @GET("api/login") @Headers("No-Authentication: true")
    Observable<Response<TokenResponse>> login(@Header("Authorization") String encodedCredentials);

    @POST("api/register") @Headers("No-Authentication: true")
    Observable<Response<TokenResponse>> register(@Body UseRequestBody userRequest);

    //User

    @GET("api/self")
    Observable<Response<CurrentUserInfoRetrofit>> getSelf();

    @GET("api/users/names")
    Observable<Response<List<UserHint>>> getAllUserNames();

    //Events

    @GET("api/events")
    Observable<Response<List<EventRetrofitObject.EventResponse>>> getAllEvents();

    @POST("api/events")
    Observable<Response<EventRetrofitObject.EventResponse>> addEvent(@Body Event event);

    @GET("api/events/self")
    Observable<Response<List<EventRetrofitObject.EventResponse>>> getOnlyUsersEvents();

    @GET("api/events/{eventId}")
    Observable<Response<EventRetrofitObject.EventResponse>> getEventById(@Path("eventId") String eventId);

    @PUT("api/events/{eventId}")
    Observable<Response<Void>> updateEvent(@Path("eventId") String eventId, @Body EventRetrofitObject.UpdateEvent event);

    @DELETE("api/events/{eventId}")
    Observable<Response<Void>> deleteEvent(@Path("eventId") String eventId);

    //Groups

    @GET("api/groups")
    Observable<Response<List<GroupResponse>>> getGroups();

    @POST("api/groups")
    Observable<Response<Void>> addNewGroup(@Body CreateGroupRequest createGroupRequest);

    @DELETE("api/groups/{groupId}")
    Observable<Response<Void>> deleteGroup(@Path("groupId") String groupId);

    @GET("api/groups/{groupId}")
    Observable<Response<GroupResponse>> getGroupDetails(@Path("groupId") String groupId);

    //Images (NOT THE SAME URL!!!)

    @Multipart
    @POST("api/images/{userId}/{eventId}")
    Observable<Response<Void>> addImage(@Path("userId")String userId, @Path("eventId") String eventId, @Part MultipartBody.Part image);

    @PUT("api/groups/{groupId}")
    Observable<Response<Void>> updateGroup(@Path("groupId") String id, @Body CreateGroupRequest group);
}
