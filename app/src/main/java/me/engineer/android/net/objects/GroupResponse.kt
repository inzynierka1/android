package me.engineer.android.net.objects

import me.engineer.android.net.ConvertibleRetrofitObject
import me.engineer.android.objects.Group

/**
 * Created by Bartosz Frankowski on 07.10.2017
 */

data class GroupResponse(val id: String,
                         val name: String,
                         val owner: GroupUserResponse,
                         val users: List<GroupUserResponse>) : ConvertibleRetrofitObject<Group> {

    override fun convert(): Group {
        return Group(id = id, name = name, members = users.map { it.toGroupMember() })
    }

    data class GroupUserResponse(val userId: String?, val email: String?) {
        fun toGroupMember(): Group.GroupMember = Group.GroupMember(userId.orEmpty(), email.orEmpty())
    }
}
