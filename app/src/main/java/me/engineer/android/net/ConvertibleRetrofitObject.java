package me.engineer.android.net;

import me.engineer.android.common.Convertible;

/**
 * Created by Bartosz Frankowski on 02.08.2017
 */

public interface ConvertibleRetrofitObject<T> extends Convertible<T> {
}
