package me.engineer.android.net

import android.util.Log
import com.facebook.stetho.okhttp3.StethoInterceptor
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response

/**
 * Created by Bartosz Frankowski on 03.10.2017
 */
class HttpClientProvider private constructor() {

    private object Holder {
        val INSTANCE = HttpClientProvider()
    }

    companion object {
        val instance: HttpClientProvider by lazy { Holder.INSTANCE }
    }

    val client: OkHttpClient = OkHttpClient.Builder()
            .addNetworkInterceptor(StethoInterceptor())
            .addInterceptor(DefaultHeaderInterceptor())
            .build()

    fun withAuth(token: String): OkHttpClient {
        return client.newBuilder()
                .addInterceptor(AuthHeaderInterceptor(token))
                .build()
    }

    private class DefaultHeaderInterceptor : Interceptor {
        override fun intercept(chain: Interceptor.Chain): Response {
            val request = chain.request().newBuilder()
                    .addHeader("Content-Type", "application/json")
                    .build()
            return chain.proceed(request)
        }
    }

    private class AuthHeaderInterceptor(
            val token: String?, val headerName: String = "X-AUTH-TOKEN") : Interceptor {
        private val TAG = AuthHeaderInterceptor::class.java.simpleName

        override fun intercept(chain: Interceptor.Chain): Response {
            fun isAuthNeeded(request: Request) = request.header("No-Authentication").isNullOrEmpty()

            val request = chain.request()

            if (!token.isNullOrEmpty() && isAuthNeeded(request)) {
                Log.d(TAG, "intercept: " + token)

                val result = request.newBuilder()
                        .addHeader(headerName, token!!)
                        .build()

                return chain.proceed(result)
            }
           return chain.proceed(request)
        }

    }

}