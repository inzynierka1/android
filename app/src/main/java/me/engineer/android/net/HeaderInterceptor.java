package me.engineer.android.net;

import android.util.Log;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by Bartosz Frankowski on 02.08.2017
 */

public class HeaderInterceptor implements Interceptor {
    public static final String TAG = HeaderInterceptor.class.getSimpleName();

    private static String authHeader;

    HeaderInterceptor(String authHeader) {
        HeaderInterceptor.authHeader = authHeader;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();
        Request.Builder requestBuilder = request.newBuilder()
                .addHeader("Content-Type", "application/json");
        if (authHeader != null && !authHeader.isEmpty()) {
            Log.d(TAG, "intercept: "+authHeader);
            requestBuilder.addHeader("X-AUTH-TOKEN", authHeader);
        }
        Log.d(TAG, "intercept: "+authHeader);
        Log.d(TAG, "intercept: "+requestBuilder.toString());

        return chain.proceed(requestBuilder.build());
    }
}
