package me.engineer.android.net.objects

import me.engineer.android.config.API
import me.engineer.android.net.ConvertibleRetrofitObject
import me.engineer.android.objects.Event
import org.joda.time.DateTime

/**
 * Created by Bartosz Frankowski on 08.11.2017
 */
sealed class EventRetrofitObject {

    data class EventResponse(
            val id: String,
            val userId: String,
            val title: String,
            val content: String,
            val timestamp: String,
            val images: List<ImageResponse>?,
            val groups: List<String>?
    ) : EventRetrofitObject(), ConvertibleRetrofitObject<Event> {

        override fun convert(): Event = Event(
                eventId = id,
                userId = userId,
                title = title,
                content = content,
                timestamp = DateTime.parse(timestamp),
                images = images.orEmpty().map {
                    Event.EventImage(
                            id = it.id,
                            name = it.name,
                            url = "${API.CONTENT_ENDPOINT}/api/images/$userId/$id/${it.id}")
                },
                groups = groups.orEmpty()
        )

        data class ImageResponse(val id: String, val name: String)
    }

    data class AddEventRequest(
            val userId: String,
            val title: String,
            val content: String,
            val groups: List<String>
    ) : EventRetrofitObject() {
        companion object {
            fun from(event: Event): AddEventRequest = AddEventRequest(
                    userId = event.userId,
                    title = event.title,
                    content = event.content,
                    groups = event.groups.orEmpty()
            )
        }
    }

    data class UpdateEvent(
            val id: String,
            val userId: String,
            val title: String,
            val content: String,
            val timestamp: String,
            val images: List<ImageResponse>?,
            val groups: List<String>?
    ) : EventRetrofitObject() {
        companion object {
            fun from(event: Event): UpdateEvent {
                return UpdateEvent(
                        id = event.eventId,
                        userId = event.userId,
                        title = event.title,
                        content = event.content,
                        timestamp = event.timestamp.toString(),
                        images = event.images.orEmpty().map { ImageResponse(it.id, it.name) },
                        groups = event.groups.orEmpty()
                )
            }
        }

        data class ImageResponse(val id: String, val name: String)
    }
}