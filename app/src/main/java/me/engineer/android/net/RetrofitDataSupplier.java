package me.engineer.android.net;

import io.reactivex.Observable;
import retrofit2.Response;

/**
 * Created by Bartosz Frankowski on 21.08.2017
 */

public interface RetrofitDataSupplier <T> {

    Observable<Response<T>> request(RestService restService);
}
