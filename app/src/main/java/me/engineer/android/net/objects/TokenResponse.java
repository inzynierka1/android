package me.engineer.android.net.objects;

import me.engineer.android.net.ConvertibleRetrofitObject;
import me.engineer.android.objects.Token;

/**
 * Created by Bartosz Frankowski on 04.09.2017
 */

public class TokenResponse implements ConvertibleRetrofitObject<Token> {
    private String token;

    @SuppressWarnings("unused")
    public TokenResponse() {
    }

    public String getToken() {
        return token;
    }

    @Override
    public Token convert() {
        return new Token(token);
    }
}
