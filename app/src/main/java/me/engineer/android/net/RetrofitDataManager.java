package me.engineer.android.net;

import android.util.Log;

import java.util.List;

import io.reactivex.Maybe;
import io.reactivex.Observable;
import io.reactivex.ObservableTransformer;
import retrofit2.HttpException;
import retrofit2.Response;

/**
 * Created by Bartosz Frankowski on 21.08.2017
 */

public class RetrofitDataManager {

    private RestService restService;
    private static final String TAG = "RetrofitDataManager";

    public RetrofitDataManager() {
        restService = RetrofitProvider.Companion.getInstance().getRetrofitInstance().create(RestService.class);
    }

    public RetrofitDataManager(RestService restService) {
        this.restService = restService;
    }

    public static RetrofitDataManager newForEndpoint(String endpoint) {
        RestService restService = RetrofitProvider.Companion.getInstance()
                .getCustomRetrofitInstance(endpoint, null).create(RestService.class);
        return new RetrofitDataManager(restService);
    }

    public static RetrofitDataManager newInstance() {
        return new RetrofitDataManager();
    }

    public <R extends ConvertibleRetrofitObject<T>,T> Observable<T> get(RetrofitDataSupplier<R> supplier) {
        return supplier.request(restService)
                .flatMap(this::observeResponse)
                .compose(handleFailedCall())
                .map(ConvertibleRetrofitObject::convert);
    }

    public <R extends ConvertibleRetrofitObject<T>,T> Observable<List<T>> getAll(RetrofitDataSupplier<List<R>> supplier) {
        return supplier.request(restService)
                .flatMap(this::observeResponse)
                .compose(handleFailedCall())
                .flatMap(Observable::fromIterable)
                .map(ConvertibleRetrofitObject::convert)
                .toList().toObservable();
    }

    public <T> Observable<T> request(RetrofitDataSupplier<T> supplier) {
        return supplier.request(restService)
                .flatMap(this::observeResponse)
                .compose(handleFailedCall());
    }

    private <T> Observable<T> observeResponse(Response<T> response){
        if(!response.isSuccessful()){
            return Observable.error(new HttpException(response));
        }
        Log.d(TAG, "Request successful: " + response.code());
        Log.d(TAG, "Request body: " + response.body());

        return response.body() != null ? Observable.just(response.body()) : Observable.empty();
    }

    private <T> ObservableTransformer<T,T> handleFailedCall() {
        return observable -> observable
                .doOnError(throwable -> {
                    if (throwable instanceof HttpException) {
                        Log.e(TAG, "Request failed with HttpError code: " + ((HttpException) throwable).code());
                    } else {
                        Log.e(TAG, "Request failed, reason: " + throwable.getMessage());
                    }
                });
    }
}
