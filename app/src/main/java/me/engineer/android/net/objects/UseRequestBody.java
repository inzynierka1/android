package me.engineer.android.net.objects;

/**
 * Created by Bartosz Frankowski on 24.08.2017
 */

public class UseRequestBody {

    private String email;
    private String password;

    public UseRequestBody() {
    }

    public UseRequestBody(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }
}
