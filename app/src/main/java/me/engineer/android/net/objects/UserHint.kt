package me.engineer.android.net.objects

/**
 * Created by Bartosz Frankowski on 02.11.2017
 */
data class UserHint(val userId: String, val email: String) {
}