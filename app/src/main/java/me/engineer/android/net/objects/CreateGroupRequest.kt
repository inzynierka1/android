package me.engineer.android.net.objects

import me.engineer.android.objects.Group

/**
 * Created by Bartosz Frankowski on 03.11.2017
 */
data class CreateGroupRequest(val name: String, val users: List<String>) {
    companion object {
        fun from(group: Group): CreateGroupRequest =
                CreateGroupRequest(group.name, group.members.map { it.id })

    }
}