package me.engineer.android.providers

import android.util.Log

import io.reactivex.Observable
import io.realm.RealmObject
import me.engineer.android.db.ConvertibleRealmObject
import me.engineer.android.db.RealmDataManager
import me.engineer.android.db.RealmRequest
import me.engineer.android.net.ConvertibleRetrofitObject
import me.engineer.android.net.RetrofitDataManager
import me.engineer.android.net.RetrofitDataSupplier
import me.engineer.android.util.RxUtil

/**
 * Created by Bartosz Frankowski on 04.09.2017
 */

class BaseDataProvider : DataProvider {
    private val realmDataManager = RealmDataManager()
    private val retrofitDataManager = RetrofitDataManager()


    override fun <R, S : ConvertibleRetrofitObject<T>, T> request(
            realmRequest: RealmRequest<R, T>, networkDataSupplier: RetrofitDataSupplier<S>): Observable<T>
            where R : RealmObject, R : ConvertibleRealmObject<T> {
        val dbResults = realmDataManager
                .get(realmRequest.realmType)
                .filter(realmRequest.filter)

        val networkResults = retrofitDataManager.get(networkDataSupplier)
                .doOnNext { a -> realmDataManager.save(a, realmRequest.realmType) }
                .doOnNext { Log.d(TAG, "Obtaining data from network succeeded.") }
                .doOnError { Log.e(TAG, "Request failed with result ${it.message}. Proceeding with db results.") }
                .onErrorResumeNext { t: Throwable -> dbResults }

        return Observable.concat(dbResults, networkResults)
    }

    override fun <R, S : ConvertibleRetrofitObject<T>, T> requestAll(
            realmRequest: RealmRequest<R, T>, networkDataSupplier: RetrofitDataSupplier<List<S>>): Observable<List<T>>
            where R : RealmObject, R : ConvertibleRealmObject<T> {

        val dbResults = getDbData(realmRequest.realmType)
                .compose(RxUtil.filterListItems(realmRequest.filter))

        val networkResults = retrofitDataManager.getAll(networkDataSupplier)
                .doOnNext { a -> realmDataManager.save(a, realmRequest.realmType) }
                .doOnNext { Log.d(TAG, "Obtaining data from network succeeded.") }
                .doOnError { Log.e(TAG, "Request failed with result ${it.message}. Proceeding with db results.") }
                .onErrorResumeNext { t: Throwable -> dbResults }

        return Observable.concat(dbResults, networkResults)

    }

    private fun <R, T> getDbData(persistenceType: Class<R>): Observable<List<T>> where R : RealmObject, R : ConvertibleRealmObject<T> {
        return realmDataManager
                .get(persistenceType)
                .toList().toObservable()
    }

    companion object {
        private val TAG = "BaseDataProvider"
    }
}
