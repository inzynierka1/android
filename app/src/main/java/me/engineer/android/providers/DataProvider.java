package me.engineer.android.providers;

import java.util.List;

import io.reactivex.Observable;
import io.realm.RealmObject;
import me.engineer.android.db.ConvertibleRealmObject;
import me.engineer.android.db.RealmRequest;
import me.engineer.android.net.ConvertibleRetrofitObject;
import me.engineer.android.net.RetrofitDataSupplier;


/**
 * Created by Bartosz Frankowski on 24.09.2017
 */

interface DataProvider {

    <R extends RealmObject & ConvertibleRealmObject<T>,
            S extends ConvertibleRetrofitObject<T>, T> Observable<T> request(
            RealmRequest<R,T> persistenceType, RetrofitDataSupplier<S> networkDataSupplier);

    <R extends RealmObject & ConvertibleRealmObject<T>,
            S extends ConvertibleRetrofitObject<T>, T> Observable<List<T>> requestAll(
            RealmRequest<R,T> persistenceType, RetrofitDataSupplier<List<S>> networkDataSupplier);
}
