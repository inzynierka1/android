package me.engineer.android.providers;

import android.util.Log;

import java.util.List;

import io.reactivex.Observable;
import io.realm.RealmObject;
import me.engineer.android.db.ConvertibleRealmObject;
import me.engineer.android.db.RealmDataManager;
import me.engineer.android.db.RealmRequest;
import me.engineer.android.net.ConvertibleRetrofitObject;
import me.engineer.android.net.RetrofitDataManager;
import me.engineer.android.net.RetrofitDataSupplier;

/**
 * Created by Bartosz Frankowski on 04.09.2017
 */

public class NetworkOnlyProviderWithSave implements DataProvider {
    public static final String TAG = NetworkOnlyProviderWithSave.class.getSimpleName();

    private RealmDataManager realmDataManager = new RealmDataManager();
    private RetrofitDataManager retrofitDataManager = new RetrofitDataManager();

    public static NetworkOnlyProviderWithSave getNew() {
        return new NetworkOnlyProviderWithSave();
    }

    @Override
    public <R extends RealmObject & ConvertibleRealmObject<T>, S extends ConvertibleRetrofitObject<T>, T> Observable<T>
    request(RealmRequest<R, T> persistenceType, RetrofitDataSupplier<S> networkDataSupplier) {
        Log.d(TAG, "Perform request: " + networkDataSupplier.toString());

        return retrofitDataManager.get(networkDataSupplier)
                .doOnNext(result -> realmDataManager.save(result, persistenceType.getRealmType()));
    }

    @Override
    public <R extends RealmObject & ConvertibleRealmObject<T>, S extends ConvertibleRetrofitObject<T>, T>
    Observable<List<T>> requestAll(RealmRequest<R, T> persistenceType, RetrofitDataSupplier<List<S>> networkDataSupplier) {
        return retrofitDataManager.getAll(networkDataSupplier)
                .doOnNext(response -> Log.d(TAG, "Request succesfull: " + response.toString()))
                .doOnNext(response -> realmDataManager.save(response, persistenceType.getRealmType()))
                .doOnError(throwable -> Log.e(TAG, "Request failed: " + throwable.getMessage()));
    }
}
