package me.engineer.android.db;

import io.reactivex.functions.Predicate;
import io.realm.RealmObject;


/**
 * Created by Bartosz Frankowski on 20.09.2017
 */

public class RealmRequest<R extends RealmObject & ConvertibleRealmObject<T>, T> {
    private Class<R> realmType;
    private Predicate<T> filter = t -> true;

    private RealmRequest() {
    }

    public RealmRequest(Class<R> realmType) {
        this.realmType = realmType;
    }

    public RealmRequest(Class<R> realmType, Predicate<T> filter) {
        this.realmType = realmType;
        this.filter = filter;
    }

    public static <R extends RealmObject & ConvertibleRealmObject<T>, T> RealmRequest<R,T> of(Class<R> type) {
        return new RealmRequest<>(type);
    }

    public static <R extends RealmObject & ConvertibleRealmObject<T>, T> RealmRequest<R,T> of(Class<R> type, Predicate<T> filter) {
        return new RealmRequest<>(type, filter);
    }

    public static <R extends RealmObject & ConvertibleRealmObject<T>, T> RealmRequest<R,T> empty() {
        return new RealmRequest<>();
    }

    public Class<R> getRealmType() {
        return realmType;
    }

    public Predicate<T> getFilter() {
        return filter;
    }

    private class EmptyRequest extends RealmRequest {
        @Override
        public Class getRealmType() {
            throw new IllegalStateException("Empty request does not have a type!");
        }
    }

}
