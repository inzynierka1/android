package me.engineer.android.db.objects;

import io.realm.RealmObject;
import io.realm.annotations.RealmClass;
import me.engineer.android.db.ConvertibleRealmObject;
import me.engineer.android.objects.Token;

/**
 * Created by Bartosz Frankowski on 02.08.2017
 */

public class RealmToken extends RealmObject implements ConvertibleRealmObject<Token>{
    private String token;

    @SuppressWarnings("unused")
    public RealmToken() {
    }

    @SuppressWarnings("unused")
    public RealmToken(String token) {
        this.token = token;
    }

    @SuppressWarnings("unused")
    public RealmToken(Token token) {this.token = token.getToken();}

    public String getToken() {
        return token;
    }

    @Override
    public Token convert() {
        return new Token(token);
    }
}
