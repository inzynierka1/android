package me.engineer.android.db.objects

import io.realm.RealmList
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import me.engineer.android.db.ConvertibleRealmObject
import me.engineer.android.db.toList
import me.engineer.android.db.toRealmList
import me.engineer.android.objects.Event
import org.joda.time.DateTime

/**
 * Created by Bartosz Frankowski on 20.09.2017
 */

open class RealmEvent() : RealmObject(), ConvertibleRealmObject<Event> {
    @PrimaryKey open var id: String = ""
    open var userId: String = ""
    open var title: String = ""
    open var content: String = ""
    open var timestamp: String = ""
    open var images: RealmList<RealmEventImage> = RealmList()
    open var groups: RealmList<String> = RealmList()
    open var currentUserIsOwner: Boolean = false

    constructor(event: Event) : this() {
        this.id = event.eventId
        this.userId = event.userId
        this.title = event.title
        this.content = event.content
        this.timestamp = event.timestamp?.toString().orEmpty()
        this.images = event.images?.map { RealmEventImage(it) }.orEmpty().toRealmList()
        this.groups = event.groups.orEmpty().toRealmList()
        this.currentUserIsOwner = event.currentUserIsOwner
    }

    override fun convert(): Event {
        return Event(id, userId, title, content, DateTime(timestamp), images.toList(), groups, currentUserIsOwner)
    }

}
