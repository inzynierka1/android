package me.engineer.android.db;

import me.engineer.android.common.Convertible;

/**
 * Created by Bartosz Frankowski on 04.07.2017
 */

public interface ConvertibleRealmObject<T> extends Convertible<T> {

}
