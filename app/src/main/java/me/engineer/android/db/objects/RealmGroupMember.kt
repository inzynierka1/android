package me.engineer.android.db.objects

import io.realm.RealmObject
import me.engineer.android.db.ConvertibleRealmObject
import me.engineer.android.objects.Group

/**
 * Created by Bartosz Frankowski on 03.11.2017
 */
open class RealmGroupMember(): RealmObject(), ConvertibleRealmObject<Group.GroupMember> {
    open var id: String? = null
    open var email: String? = null

    constructor(groupMember: Group.GroupMember) : this() {
        this.id = groupMember.id
        this.email = groupMember.email
    }


    override fun convert(): Group.GroupMember = Group.GroupMember(id.orEmpty(),email.orEmpty())
}