package me.engineer.android.db.objects

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import me.engineer.android.db.ConvertibleRealmObject
import me.engineer.android.objects.CurrentUserInfo

/**
 * Created by Bartosz Frankowski on 08.11.2017
 */
open class RealmCurentUserInfo(): RealmObject(), ConvertibleRealmObject<CurrentUserInfo> {
    @PrimaryKey open var id = "1"
    open var userId = ""
    open var email = ""

    constructor(userInfo: CurrentUserInfo): this() {
        userId = userInfo.id
        email = userInfo.email
    }

    override fun convert(): CurrentUserInfo = CurrentUserInfo(userId, email)

}