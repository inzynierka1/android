package me.engineer.android.db;

import io.realm.Realm;

/**
 * Created by Bartosz Frankowski on 04.07.2017
 */

public class RealmService {

    private Realm realm;
    private static RealmService instance;

    public static RealmService getInstance() {
        return instance!=null ? instance : new RealmService();
    }

    public RealmService() {
        realm = Realm.getDefaultInstance();
    }

    public void clearSession() {
        realm.beginTransaction();

        realm.deleteAll();

        realm.commitTransaction();
        realm.close();
    }
}
