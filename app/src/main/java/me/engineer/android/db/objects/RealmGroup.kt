package me.engineer.android.db.objects

import io.realm.RealmList
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import me.engineer.android.db.ConvertibleRealmObject
import me.engineer.android.objects.Group

/**
 * Created by Bartosz Frankowski on 19.10.2017
 */
open class RealmGroup(): RealmObject(), ConvertibleRealmObject<Group> {
    @PrimaryKey open var id: String? = null
    open var name: String? = null
    open var members: RealmList<RealmGroupMember>? = null

    constructor(group: Group) : this() {
        id = group.id
        name = group.name
        val realmMembers = group.members.map { RealmGroupMember(it) }.iterator()
        val list = RealmList(realmMembers.next())
        while (realmMembers.hasNext()) list.add(realmMembers.next())
        members = list
    }

    override fun convert(): Group = Group(id.orEmpty(), name.orEmpty(), members?.toList()?.map { it.convert() }.orEmpty())

}
