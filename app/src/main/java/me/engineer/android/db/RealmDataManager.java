package me.engineer.android.db;

import android.support.annotation.NonNull;
import android.util.Log;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmObject;
import io.realm.RealmResults;
import me.engineer.android.common.Convertible;

/**
 * Created by Bartosz Frankowski on 02.08.2017
 */

public class RealmDataManager {

    public static final String TAG = RealmDataManager.class.getSimpleName();
    private RealmConfiguration configuration = Realm.getDefaultInstance().getConfiguration();

    public static RealmDataManager getNew() {
        return new RealmDataManager();
    }

    public <R extends RealmObject & ConvertibleRealmObject<T>, T> Observable<T> get(Class<R> realmClass) {
        return Observable.fromIterable(retrieveDataFromRealm(realmClass))
                .filter(t -> t != null)
                .map(r -> r.convert());
    }

    public <R extends RealmObject & Convertible<T>, T> Observable<T> get(String id, Class<R> realmClass) {
        return Observable.just(retrieveDataFromRealm(id, realmClass))
                .filter(t -> t != null)
                .map((r) -> r.convert());
    }

    private <T extends RealmObject> List<T> retrieveDataFromRealm(Class<T> tClass) {
        Realm realm = Realm.getInstance(configuration);
        RealmResults<T> realmResults = realm.where(tClass).findAll();
        List<T> results = realm.copyFromRealm(realmResults);
        realm.close();

        Log.d(TAG, "Retrieving " + tClass + " from Realm. Found: " + results.size());
        return results;
    }

    private <T extends RealmObject> T retrieveDataFromRealm(String id, Class<T> tClass) {
        T result = null;
        Realm realm = Realm.getInstance(configuration);

        T realmResult = realm.where(tClass).equalTo("id", id).findFirst();
        if (realmResult != null) {
            result = realm.copyFromRealm(realmResult);
        }

        realm.close();

        Log.d(TAG, "Retrieving " + tClass + " with 'id':" + id + " from Realm. Found: " + (result != null));
        return result;
    }

    public <T extends RealmObject> void removeFromRealm(String id, Class<T> tClass) {
        Realm realm = Realm.getInstance(configuration);
        realm.beginTransaction();
        T result = realm.where(tClass).equalTo("id", id).findFirst();
        if (result != null && result.isValid()) {
            result.deleteFromRealm();
        }
        realm.commitTransaction();
        realm.close();
    }

    public <R extends RealmObject & Convertible<T>, T> void save(List<T> data, Class<R> realmClass) {
        if (data.size() > 0) {
            Log.d(TAG, "Caching in realm: mapping from " + data.get(0).getClass().getSimpleName() + " to " + realmClass.getSimpleName());
            try {
                Constructor<R> realmObjConstructor = realmClass.getConstructor(data.get(0).getClass());

                List<R> realmObjectsList = new ArrayList<>();
                for (T regularObject : data) {
                    realmObjectsList.add(realmObjConstructor.newInstance(regularObject));
                }
                saveRealmData(realmObjectsList);
            } catch (NoSuchMethodException e) {
                Log.e(TAG, "Exception occured when saving observable data to realm.");
                Log.e(TAG, realmClass.getSimpleName() + " class must declare constructor of type " + realmClass.getSimpleName() + "(" + data.get(0).getClass().getSimpleName() + ").");
            } catch (IllegalAccessException | InvocationTargetException | InstantiationException e) {
                Log.e(TAG, "Exception occured when saving to realm: " + e.getMessage());
                e.printStackTrace();
            }
        }
    }

    public <R extends RealmObject & Convertible<T>, T> void save(@NonNull T data, Class<R> realmClass) {
        Log.d(TAG, "Caching in realm: mapping from " + data.getClass().getSimpleName() + " to " + realmClass.getSimpleName());
        try {
            Constructor<R> realmObjConstructor = realmClass.getConstructor(data.getClass());

            List<R> realmObjectsList = new ArrayList<>();
            realmObjectsList.add(realmObjConstructor.newInstance(data));
            saveRealmData(realmObjectsList);
        } catch (NoSuchMethodException e) {
            Log.e(TAG, "Exception occured when saving observable data to realm.");
            Log.e(TAG, realmClass.getSimpleName() + " class must declare constructor of type " + realmClass.getSimpleName() + "(" + data.getClass().getSimpleName() + ").");
        } catch (IllegalAccessException | InvocationTargetException | InstantiationException e) {
            Log.e(TAG, "Exception occured when saving to realm: " + e.getMessage());
        }
    }

    private <R extends RealmObject> void saveRealmData(List<R> realmObjects) {
        try (Realm realm = Realm.getInstance(configuration)) {
            realm.executeTransaction(r -> r.insertOrUpdate(realmObjects));
        }
    }

    public void cleanup() {
        Realm realm = Realm.getInstance(configuration);
        realm.beginTransaction();
        realm.deleteAll();
        realm.commitTransaction();
        realm.close();
    }
}
