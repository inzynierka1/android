package me.engineer.android.db.objects

import io.realm.RealmObject
import me.engineer.android.db.ConvertibleRealmObject
import me.engineer.android.objects.Event

/**
 * Created by Bartosz Frankowski on 08.11.2017
 */
open class RealmEventImage() : RealmObject(), ConvertibleRealmObject<Event.EventImage> {
    open var id: String = ""
    open var name: String = ""
    open var url: String = ""
    override fun convert(): Event.EventImage = Event.EventImage(id, url, name)

    constructor(eventImage: Event.EventImage): this() {
        this.id = eventImage.id
        this.url = eventImage.url
        this.name = eventImage.name
    }
}