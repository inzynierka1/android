package me.engineer.android.db

import io.realm.RealmList

/**
 * Created by Bartosz Frankowski on 08.11.2017
 */
fun <E> List<E>.toRealmList(): RealmList<E> {
    val iterator = iterator()
    val result = RealmList<E>()
    while (iterator.hasNext()) result.add(iterator.next())
    return result
}

fun <T, E: ConvertibleRealmObject<T>> RealmList<E>.toList(): List<T> {
    return this.map { it.convert() }.toList()
}