package me.engineer.android;

import android.app.Application;

import com.facebook.stetho.Stetho;
import com.squareup.picasso.OkHttpDownloader;
import com.squareup.picasso.Picasso;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by bartoszfrankowski on 04.07.2017
 */
public class App extends Application {
    private static App instance;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;

        initRealm();
        initPicasso();
        initConfig();
    }

    private void initPicasso() {
        Picasso picasso = new Picasso.Builder(this)
                .downloader(new OkHttpDownloader(this, Integer.MAX_VALUE))
                .build();
//        For picasso debug
//        picasso.setIndicatorsEnabled(true);
//        picasso.setLoggingEnabled(true);
        Picasso.setSingletonInstance(picasso);

    }

    private void initRealm() {
        Realm.init(this);
        RealmConfiguration configuration = new RealmConfiguration.Builder()
                .name(getPackageName() + ".realm")
                .deleteRealmIfMigrationNeeded()
                .build();

        Realm.setDefaultConfiguration(configuration);
    }

    private void initConfig() {
        Stetho.initializeWithDefaults(this);
    }

    public static App getInstance() {
        return instance;
    }
}
